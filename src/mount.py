#!/usr/bin/env python

"""
Mount driver wrappers

@author: Kevin Allekotte
"""

import abc
import time
from lib import indiclient


class Mount:
    """Mount driver Interface"""
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        """Initialize mount"""
        self.setup()

    @abc.abstractmethod
    def setup(self):
        """Initialize mount"""
        raise NotImplementedError

    @abc.abstractmethod
    def moveto(self, ra, dec):
        """Set coordinates"""
        raise NotImplementedError

    @abc.abstractmethod
    def __del__(self):
        """Mount exit"""
        raise NotImplementedError


class MountFailure(Exception):
    """Mount operation failed."""


class TemmaIndiMount(Mount):
    """Temma takahashi client wrapper"""

    cli = None

    def set_text(self, vector, prop, val):
        """Set text property"""
        print 'set_and_send_text("Temma", %s, %s, %s)' % (vector, prop, val)
        self.cli.set_and_send_text("Temma", vector, prop, val)

    def setf(self, vector, prop, val):
        """Set float property"""
        print 'set_and_send_float("Temma", %s, %s, %f)' % (vector, prop, val)
        return self.cli.set_and_send_float("Temma", vector, prop, val)

    def getf(self, vector, prop):
        """Get float property"""
        return self.cli.get_float("Temma", vector, prop)

    def get_text(self, vector, prop):
        """Get text property"""
        return self.cli.get_text("Temma", vector, prop)

    def set_switch(self, prop, val):
        """Switch property type"""
        print 'set_and_send_switchvector_by_elementlabel("Temma", %s, %s)' % \
            (prop, val)
        self.cli.set_and_send_switchvector_by_elementlabel("Temma", prop, val)

    def setup(self):
        """Connect to Temma mount"""
        try:
            self.cli = indiclient.indiclient("localhost", 7624)
            time.sleep(1)
            self.set_text("DEVICE_PORT", "PORT", "/dev/ttyUSB0")
            self.set_switch("CONNECTION", "On")
            self.set_switch("RA motor", "On")
            self.set_switch("ON_COORD_SET", "Goto")
            time.sleep(1)
            assert(self.get_text("CONNECTION", "CONNECT") == "On")
        except Exception:
            raise MountFailure("Mount Initialization failed")

    def moveto(self, ra, dec):
        """Set RA/DEC coords"""
        try:
            self.setf("EQUATORIAL_EOD_COORD", "RA", ra).wait_for_ok_timeout(60.)
            self.setf("EQUATORIAL_EOD_COORD", "DEC", dec)\
                .wait_for_ok_timeout(60.)
            newra = self.getf("EQUATORIAL_COORD","RA")
            newdec = self.getf("EQUATORIAL_COORD","DEC")
        except Exception:
            raise MountFailure("Could not move to %f, %f" % (ra, dec))
        return newra, newdec

    def __del__(self):
        """Quit"""
        self.cli.quit()


class FakeMount(Mount):
    """A fake mount that does nothing."""

    def setup(self):
        """nothing"""
        print "FakeMount is On"

    def moveto(self, ra, dec):
        """Set RA/DEC coords"""
        print "FakeMount moving... %f %f" % (ra, dec)
        time.sleep(.5)

    def __del__(self):
        """Quit"""
