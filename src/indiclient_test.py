#!/usr/bin/env python

"""
Test INDIclient

@author: Kevin Allekotte
"""

from lib import indiclient


def main():
    """Conectar al telescopio y ejecutar una tarea basica"""
    indi = indiclient.indiclient("localhost", 7624)
    indi.set_and_send_text("Temma", "DEVICE_PORT", "PORT", "/dev/ttyUSB0")
    indi.set_and_send_switchvector_by_elementlabel("Temma", "CONNECTION", "On")
    indi.set_and_send_switchvector_by_elementlabel("Temma", "RA motor", "On")
    indi.set_and_send_switchvector_by_elementlabel(
        "Temma", "ON_COORD_SET", "Goto")
    indi.set_and_send_float("Temma", "EQUATORIAL_EOD_COORD", "RA", 22.)
    indi.set_and_send_float("Temma", "EQUATORIAL_EOD_COORD", "RA", 20.)
    indi.quit()

if __name__ == "__main__":
    main()
