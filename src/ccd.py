#!/usr/bin/env python

"""
CCD driver wrappers

@author: Kevin Allekotte
"""

import os
import abc
import time
import shutil
import starsynth
import subprocess


class CCD:
    """CCD driver Interface"""
    __metaclass__ = abc.ABCMeta

    def __init__(self, outdir=None, **args):
        """Create output folder and initialize CCD"""
        self._outdir = None
        if outdir is not None:
            self.set_dir(outdir)
        self.setup(outdir, **args)

    def set_dir(self, outdir):
        """set output directory for FITS images"""
        if not os.path.isdir(outdir):
            os.mkdir(outdir)
        self._outdir = outdir

    @abc.abstractmethod
    def setup(self, outdir):
        """CCD initialization"""
        raise NotImplementedError

    @abc.abstractmethod
    def expose(self, filename, seconds, header=None):
        """Get snapshot"""
        raise NotImplementedError

    @abc.abstractmethod
    def status(self):
        """CCD status"""
        raise NotImplementedError

    @abc.abstractmethod
    def __del__(self):
        """CCD exit"""
        raise NotImplementedError


class CCDFailure(Exception):
    """CCD operation failed."""


class ApogeeCCD(CCD):
    """Apogee CCD client wrapper"""

    def client(self, command):
        """Call client"""
        print "apogeeclient", command
        return subprocess.check_output([self.apogeeclient] + command.split())

    def __init__(self, **args):
        """SynthCCD init"""
        self.apogeeclient = args.get("apogeeclient",
                                     "/opt/apogee/scripts/apogeeclient")
        super(ApogeeCCD, self).__init__(**args)

    def setup(self, outdir, **_):
        """Initialize CCD"""
        print "Connecting to ApogeeCCD"
        try:
            stat = self.status()
            time.sleep(1)
            # assert(stat..)
            print stat
        except Exception:
            raise CCDFailure("Apogee client failed")

    def set_dir(self, outdir):
        super(ApogeeCCD, self).set_dir(outdir)
        self.client("datadir %s" % outdir)

    def expose(self, filename, seconds, header=None):
        """Get a snapshot, sleep until done"""
        if header is not None:
            valid = "ra dec equinox latitude longitude name observer "\
                    "telescope instrument target filtername site".split()
            for key, value in header.items():
                if key in valid:
                    self.client("setinfo %s %s" % (key, value))
        name = os.path.abspath(filename)
        tmpname = "/tmp/apogee_expose"
        self.client("snapsleep %s %f 1" % ("../.." + tmpname, seconds))
        shutil.copyfile(tmpname + ".fits", name)

    def status(self):
        """Print status"""
        return self.client("status")

    def __del__(self):
        pass


class SynthCCD(CCD):
    """Fake image generator"""

    def __init__(self, **args):
        """SynthCCD init"""
        self.synth = starsynth.StarSynth(random=True)
        self.starsgenerated = False
        self.starttime = None
        self.write_astrometry_header = args.get("write_astrometry_header",
                                                False)
        self.sleep = args.get("sleep", True)
        super(SynthCCD, self).__init__(**args)

    def setup(self, outdir, **args):
        """Initialize Synthetic CCD"""
        mag = args.get("mag", 1500.)
        size = args.get("size", 2.)
        speed = args.get("speed", 100.)
        self.synth.add_object(mag, size, speed)

    def expose(self, filename, seconds, header=None):
        """Get a snapshot, sleep until done"""
        if header is None or "ra" not in header or "dec" not in header:
            raise CCDFailure("No RA/DEC for exposure")
        if not self.starsgenerated:
            self.synth.stars_from_catalog()
            print "SynthCCD init with %d stars from catalog" % \
                  len(self.synth.stars)
            self.starsgenerated = True
        if self.starttime is None:
            self.starttime = header["time"]
        frame = self.synth.gen_sample(header["time"] - self.starttime,
                                      (header["ra"], header["dec"]))
        head = {"JUL-DATE": header["time"]}
        astroheader = None
        if self.write_astrometry_header:
            astroheader = header["ra"], header["dec"], 20.
        starsynth.makeFITS(filename, frame, head, astroheader)
        print "SynthCCD exposing.. %s" % filename
        if self.sleep:
            print "simul sleep"
            time.sleep(seconds)

    def status(self):
        """Print status"""

    def __del__(self):
        pass
