#!/usr/bin/env python
#pylint: disable-all

import pyx
import numpy
import displayutil
import tesisutil
import starsynth
import features
import pyfits
import os

"""
"""

def graf_reduccion():
    ss = starsynth.StarSynth()
    ss.stars_from_catalog()
    cant = len(ss.stars)
    print cant
    samples = 100
    fname = "/tmp/synth.fits"
    ss.noise = {"lvl": 0, "var": 0}
    noises = numpy.logspace(2.18, 4.1, samples)
    pdao, psex = [], []
    for noise in noises:
        ss.bnoise = {"lvl": noise, "var": 150.}
        fsex, fdao = 0., 0.
        for _ in range(4):
            d = ss.gen_sample(0)
            try: os.unlink(fname)
            except: pass
            starsynth.makeFITS(fname, d, {})
            fsex += len(features.store_and_get(fname, method="sex", force=True)) / 4.
            fdao += len(features.store_and_get(fname, method="dao", force=True)) / 4.
        print fsex, fdao
        psex.append(fsex * 100. / cant)
        pdao.append(fdao * 100. / cant)
    print noises
    print pdao
    print psex
    numpy.savetxt("output/reduccion_dao_sex.dat", numpy.vstack((noises, pdao, psex)), "%12.6g", ",")
    plot_graf_reduccion("output/reduccion_dao_sex.dat")

def plot_graf_reduccion(dat):
    noises, pdao, psex = numpy.loadtxt(dat, delimiter=",")

    tesisutil.pyx_plot(numpy.vstack((noises, pdao)).T, oname="output/reduccion_dao.eps", xlog=True)
    tesisutil.pyx_plot(numpy.vstack((noises, psex)).T, oname="output/reduccion_sex.eps", xlog=True)

    g = pyx.graph.graphxy(width=12, x=pyx.graph.axis.log(max=20000., title="noise"), y=pyx.graph.axis.lin(title="\% of detected features"), key=pyx.graph.key.key(pos="tr", dist=0.1))
    data = [pyx.graph.data.points(numpy.vstack((noises, pdao)).T, x=1, y=2, title="DAOPHOT"),
            pyx.graph.data.points(numpy.vstack((noises, psex)).T, x=1, y=2, title="SExtractor")]
    g.plot(data, [pyx.graph.style.line([pyx.color.gradient.Rainbow])])
    g.writeEPSfile("output/reduccion_dao_sex.eps")


def graf_time_reduccion():
    import time
    samples = 20
    fname = "/tmp/synth.fits"
    sizes = map(int, numpy.linspace(100, 4000, samples))
    tdao, tsex, tdao2 = [], [], []
    for size in sizes:
        ss = starsynth.StarSynth(size=size)
        ss.stars_from_random(nstars=size**2/500)
        d = ss.gen_sample(0)
        try: os.unlink(fname)
        except: pass
        starsynth.makeFITS(fname, d, {})
        start = time.time()
        features.store_and_get(fname, method="sex", force=True)
        fsex =  (time.time() - start)
        start = time.time()
        features.store_and_get(fname, method="dao", force=True)
        fdao =  (time.time() - start)
        start = time.time()
        features.store_and_get(fname, method="dao", force=True, daoparams={"getflux":False})
        fdao2 =  (time.time() - start)
        print fsex, fdao, fdao2
        tsex.append(fsex)
        tdao.append(fdao)
        tdao2.append(fdao2)
    print sizes
    print tdao
    print tdao2
    print tsex
    numpy.savetxt("output/reduccion_time_dao_sex.dat", numpy.vstack((sizes, tdao, tdao2, tsex)), "%12.6g", ",")
    plot_graf_reduccion_time("output/reduccion_time_dao_sex.dat")

def plot_graf_reduccion_time(dat):
    sizes, tdao, tdao2, tsex = numpy.loadtxt(dat, delimiter=",")

    g = pyx.graph.graphxy(width=12, x=pyx.graph.axis.lin(title="size"), y=pyx.graph.axis.lin(title="time (s)"), key=pyx.graph.key.key(pos="tl", dist=0.1))
    data = [pyx.graph.data.points(numpy.vstack((sizes, tdao)).T, x=1, y=2, title="DAOPHOT"),
            pyx.graph.data.points(numpy.vstack((sizes, tdao2)).T, x=1, y=2, title="DAOPHOT (sin FLUX)"),
            pyx.graph.data.points(numpy.vstack((sizes, tsex)).T, x=1, y=2, title="SExtractor")]
    g.plot(data, [pyx.graph.style.line([pyx.color.gradient.Rainbow])])
    g.writeEPSfile("output/reduccion_time_dao_sex.eps")


def graf_time2_reduccion():
    import time
    samples = 30
    fname = "/tmp/synth.fits"
    stars = map(int, numpy.linspace(10, 10000, samples))
    tdao, tsex, tdao2 = [], [], []
    for star in stars:
        ss = starsynth.StarSynth(size=1000)
        ss.stars_from_random(nstars=star)
        d = ss.gen_sample(0)
        try: os.unlink(fname)
        except: pass
        starsynth.makeFITS(fname, d, {})
        start = time.time()
        features.store_and_get(fname, method="sex", force=True)
        fsex =  (time.time() - start)
        start = time.time()
        features.store_and_get(fname, method="dao", force=True)
        fdao =  (time.time() - start)
        start = time.time()
        features.store_and_get(fname, method="dao", force=True, daoparams={"getflux": False})
        fdao2 =  (time.time() - start)
        print fsex, fdao, fdao2
        tsex.append(fsex)
        tdao.append(fdao)
        tdao2.append(fdao2)
    print stars
    print tdao
    print tdao2
    print tsex
    numpy.savetxt("output/reduccion_time2_dao_sex.dat", numpy.vstack((stars, tdao, tdao2, tsex)), "%12.6g", ",")
    plot_graf_reduccion_time2("output/reduccion_time2_dao_sex.dat")

def plot_graf_reduccion_time2(dat):
    sizes, tdao, tdao2, tsex = numpy.loadtxt(dat, delimiter=",")

    g = pyx.graph.graphxy(width=12, x=pyx.graph.axis.lin(title="\# of stars"), y=pyx.graph.axis.lin(title="time (s)"), key=pyx.graph.key.key(pos="tl", dist=0.1))
    data = [pyx.graph.data.points(numpy.vstack((sizes, tdao)).T, x=1, y=2, title="DAOPHOT"),
            pyx.graph.data.points(numpy.vstack((sizes, tdao2)).T, x=1, y=2, title="DAOPHOT (sin FLUX)"),
            pyx.graph.data.points(numpy.vstack((sizes, tsex)).T, x=1, y=2, title="SExtractor")]
    g.plot(data, [pyx.graph.style.line([pyx.color.gradient.Rainbow])])
    g.writeEPSfile("output/reduccion_time2_dao_sex.eps")


def graf_fotometria(seq):
    import align, random
    aligned = align.AlignedSeq(seq)
    acum_points = numpy.empty((0,3), numpy.float32)
    counts = numpy.empty(0, numpy.int8)
    fluxs = []
    for frame, (fits, transform) in enumerate(aligned.iterate()):
        feats = features.store_and_get(fits)
        transformed_pts = align.apply_transform(feats[:,:2], transform)
        points = numpy.column_stack((transformed_pts, feats[:,2]))
        # Matchearlos con los puntos anteriores
        indices, dists = align.match(points[:,:2].copy(),
                                     acum_points[:,:2].copy())
        # Si no tiene un match suficientemente cerca, es un punto "nuevo"
        ok = dists >= 2.
        new = points[ok]
        # Cantidad de puntos nuevos
        n_new = new.shape[0]
        # Agregar los nuevos a la lista
        acum_points = numpy.vstack((acum_points, new))
        # Para los puntos "repetidos", aumentar el contador
        counts[numpy.unique(indices[numpy.logical_not(ok)])] += 1
        for i in numpy.nonzero(numpy.logical_not(ok))[0]:
            fluxs[indices[i]].append(points[i, 2])
        for n in new:
            fluxs.append([n[2]])
        # Poner el contador de los puntos nuevos en 1
        counts = numpy.concatenate((counts, numpy.ones(n_new)))
    m40 = [f[:20] for f in fluxs if len(f) > 20 and f[0] > 10.]
    g = pyx.graph.graphxy(width=12, x=pyx.graph.axis.lin(title="frame"), y=pyx.graph.axis.log(title="flux"))
    data = [pyx.graph.data.points(zip(range(1, 41), map(lambda x: max(.0000000000001, x), f)), x=1, y=2) for f in random.sample(m40, 20)]
    g.plot(data, [pyx.graph.style.line([pyx.color.gradient.Rainbow])])
    g.writeEPSfile("output/fotometria.eps")
    

def graf_align():
    import align, cv2
    norm = numpy.random.normal
    s = 4096
    noises = numpy.linspace(0.0001, 1., 100)
    percents = {}
    types = [["angle", "translation", "feature displacement"][1]]
    for i, typ in enumerate(types):
        i+=1
        percents[typ] = []
        for noise in noises:
            percent = 0.
            for _ in range(30):
                angle = [0., norm(0., noise * 50.)][i == 0]
                tr = cv2.getRotationMatrix2D((s / 2., s / 2.), angle, 1.)
                tr[:,2] += [(0., 0.), (norm(0., noise * 150.), norm(0., noise * 150.))][i == 1]
                src = numpy.random.random((100, 2)).astype(numpy.float32) * s
                dst = align.apply_transform(src, tr) + [numpy.zeros((100,2)), norm(0., noise * 6., (100, 2))][i == 2]
                try:
                    align.ICP(src.astype(numpy.float32).copy(), dst.astype(numpy.float32).copy())
                    percent += 100./30.
                except align.NoAlignmentFound:
                    pass
            percents[typ].append(percent)
    print percents
    for i, typ in enumerate(types):
        g = pyx.graph.graphxy(width=12, x=pyx.graph.axis.lin(title=typ), y=pyx.graph.axis.lin(title="\% of aligned frames", max=105, min=0))
        data = [pyx.graph.data.points(zip(noises * [50., 150., 6.][i+1], percents[typ]), x=1, y=2)]
        g.plot(data, [pyx.graph.style.line([pyx.color.gradient.Rainbow])])
        g.writePDFfile("output/align_%s.pdf" % typ)

def graf_deteccion(method="sex"):
    import pathfinder, shutil
    ss = starsynth.StarSynth(size=400, noise={"lvl": 0., "var": 0.})
    ss.stars_from_random(nstars=80)
    mags = numpy.linspace(1400., 2500., 30)
    ps = []
    for mag in mags:
        p = 0.
        for _ in range(10):
            ss.objects = []
            ss.add_object(mag, 1.8, speed=200.)
            try: shutil.rmtree("synth/tmptmp")
            except: pass
            ss.gen_seq("tmptmp", 10, 2454321., 1. / 86400.)
            os.chdir("../../")
            features.seq_find("synth/tmptmp/tmptmp", method=method)
            try:
                if len(pathfinder.findpath("synth/tmptmp/tmptmp")) > 0:
                    p += 100./10.
            except: pass
        ps.append(p)
        print mag, p
    print ps
    g = pyx.graph.graphxy(width=12, x=pyx.graph.axis.lin(title="object's flux"), y=pyx.graph.axis.lin(title="\% success", max=105, min=0))
    data = [pyx.graph.data.points(zip(mags, ps), x=1, y=2)]
    g.plot(data, [pyx.graph.style.line([pyx.color.gradient.Rainbow])])
    g.writePDFfile("output/deteccion.pdf")


def graf_astrometry():
    #XXX dibujar estrellas de dufR_0042 vs USNO
    import starcatalog, astrometry, cv2, align
    s = 1000
    ra, dec = numpy.random.random()*360., numpy.random.random()*180. - 90.
    ras, decs, _ = starcatalog.usno(ra, dec, 20., 100).T
    ras = (ras - ra) * .89 + ra  # Correccion USNO (?)
    xs = (ras - ra) * 60. * s / 20. + s / 2.
    ys = (decs - dec) * 60. * s / 20. + s / 2.
    feats = numpy.vstack((xs, ys)).T
    angle = numpy.random.normal(0., 50.)
    tr = cv2.getRotationMatrix2D((s / 2., s / 2.), angle, 1.)
    align.apply_transform(feats, tr)
    x0, y0, ra0, dec0, cd = astrometry.get_astrometry(feats=feats)
    print x0, y0, ra0, dec0
    print cd

