#!/usr/bin/env python

"""
Alignment of FITS sequences.

@author: Kevin Allekotte
"""

from checkimport._cv2 import cv2
from checkimport._numpy import numpy
import math
import features
import os
import sys
import readfits
import hashlib
import pickle
import random


def store_and_get(seq, force=False, debug=False):
    """Get alignment of image sequence (with cache)"""
    storename = os.path.splitext(seq)[0] + ".align"
    if not force and os.path.isfile(storename):
        if debug:
            print "Alignment is in cache: %s\n" % storename
        store = open(storename, 'rb')
        data = pickle.load(store)
        transforms = data["transforms"]
        store.close()
    else:
        files = readfits.Sequence(seq)
        transforms = seq_align(files)
        data = {"seq": seq, "transforms": transforms}
        store = open(storename, 'wb')
        pickle.dump(data, store)
        store.close()
        if debug:
            print "Alignment sotred in %s" % storename
    return transforms


def seq_align(files):
    """Align a Sequence of files"""
    transforms = []
    acum_transform = numpy.identity(3)[:2]
    prevpoints = None
    for fi in files.iterate():
        alignment = True
        status = "ok"
        feats = features.store_and_get(fi.filename)
        top_feats = feats[-80:]  # feats ya ordenados
        points = top_feats[:,:2]
        if prevpoints is not None:
            try:
                transform = ICP(points, prevpoints)
                acum_transform = stack_transform(acum_transform, transform)
            except NoAlignmentFound:
                alignment = False
                status = "FAIL"
        if alignment:
            prevpoints = points.copy()
            transforms.append(acum_transform.copy())
        else:
            transforms.append(None)
        print "%s: %s" % (os.path.basename(fi.filename), status)
    return transforms


def ICP(src, dst, update_dst=False, thresh=2.):
    """Find rigid transformation that fits src to dst"""
    FLANN_INDEX_KDTREE = 1  # bug: flann enums are missing
    flann_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=2)
    flann_index = cv2.flann_Index(dst, flann_params)
    lastDist = float("inf")
    transform = numpy.identity(3)[:2]
    for _ in range(10):
        new_src = apply_transform(src, transform)
        # ..or params={'checks': 64} ?
        indices, dists = flann_index.knnSearch(new_src, 1, params={})
        # src[i] is near dst[indices[i]]
        dist = numpy.median(dists)
        if dist >= lastDist:
            break
        lastDist = dist
        # ordered_dst = dst[indices.flatten()]
        ordered_dst = numpy.array([dst[index] for index in indices.flatten()])
        # src[i] matches ordered_dst[i]
        m = find_transform_ransac(src, ordered_dst)
        if m is None:
            raise NoAlignmentFound
        # update transformation
        transform = m
    if update_dst:
        new_src = apply_transform(src, transform)
        indices, dists = flann_index.knnSearch(new_src, 1, params={})
        ordered_dst = dst[indices.flatten()]
        diffs = ordered_dst - new_src
        dists = numpy.sqrt(numpy.sum(numpy.abs(diffs) ** 2, axis=-1))
        matches = dists < thresh
        # matching_indices = indices[matches].flatten()
        # Average matching srcs and dsts
        # dst[matching_indices] = (src[matches] + dst[matching_indices]) / 2.
        # Append non-marching srcs
        dst = numpy.append(dst, src[matches ^ True], axis=0)
        return (transform, dst)
    else:
        return transform


def find_transform(src, dst):
    """Find rigid transformation that best matches each src[i] to dst[i]"""
    # Use legacy cv.EstimateRigidTransform because cv2 is buggy
    rows = src.shape[0]
    src_cv = cv2.cv.CreateMatHeader(rows, 1, cv2.cv.CV_32FC2)
    dst_cv = cv2.cv.CreateMatHeader(rows, 1, cv2.cv.CV_32FC2)
    cv2.cv.SetData(src_cv, src.tostring(), 8)
    cv2.cv.SetData(dst_cv, dst.tostring(), 8)
    M = cv2.cv.CreateMat(2, 3, cv2.cv.CV_32F)
    cv2.cv.EstimateRigidTransform(src_cv, dst_cv, M, False)
    m = numpy.fromstring(M.tostring(), dtype=numpy.float32).reshape(2,-1)
    return m


def stack_transform(t1, t2):
    """Return transformation given by applying t1, t2 sequentially"""
    t2_3x3 = numpy.resize(t2, (3,3))
    t2_3x3[2,:] = [0, 0, 1]
    return numpy.dot(t1, t2_3x3)[:2,:]


def find_transform_ransac(src, dst, thresh=2., inliers=.4):
    """Find rigid t that best matches each src[i] to dst[i] using RANSAC.

    Args:
        src: array. Points of the source image.
        dst: array. Correspondances of the src points in the target image.
        thresh: float. The acceptable distance to consider as inlier.
        inliers: float. The proportion of inliers to consider as good match.

    Returns:
        transform: the rigid transformation that best matches each src to dst.
    """
    npoints = src.shape[0]
    best = (float("inf"), None)
    for _ in range(100):
        # i1, i2 two random indices
        i1, i2 = random.sample(range(npoints - 1), 2)
        src1, src2 = src[i1], src[i2]
        dst1, dst2 = dst[i1], dst[i2]

        # Find transformation t: t(src1)->dst1 and t(src2)->dst2
        # Discard if not far enough:
        if length(src2 - src1) < 20 * thresh:
            continue
        # Discard if pairs of points don't have same distance (bad match)
        if abs(length(src2 - src1) - length(dst2 - dst1)) > thresh:
            continue
        rotation = angle(src2 - src1, dst2 - dst1)
        sin, cos = math.sin(rotation), math.cos(rotation)
        transform = numpy.array([[cos,-sin,0.], [sin,cos,0.]])
        translation = dst1 - apply_transform(numpy.array([src1]), transform)
        transform[:,2] = translation

        # Evaluate transformation
        newsrc = apply_transform(src, transform)
        dists = numpy.sqrt(numpy.sum(numpy.abs(dst - newsrc) ** 2, axis=-1))
        numinliers = numpy.sum(dists < thresh)
        if numinliers > inliers * npoints:
            error = numpy.median(dists[dists < thresh])
            if error < best[0]:
                best = (error, transform)

    return best[1]


def find_transform_move(src, dst):
    """Find translation that best matches each src[i] to dst[i]"""
    diffs = dst - src
    mean = numpy.mean(diffs, axis=0)
    hx, binsx = numpy.histogram(diffs[:,0], bins=30,
        range=(mean[0] - 20, mean[0] + 20), density=True)
    hy, binsy = numpy.histogram(diffs[:,1], bins=30,
        range=(mean[1] - 20, mean[1] + 20), density=True)
    maxbinx = numpy.argmax(hx)
    maxbiny = numpy.argmax(hy)
    modex = (binsx[maxbinx] + binsx[maxbinx + 1]) / 2.
    modey = (binsy[maxbiny] + binsy[maxbiny + 1]) / 2.
    mode = numpy.array([modex, modey])
    dists = numpy.sqrt(numpy.sum(numpy.abs(diffs - mode) ** 2, axis=-1))
    inliers = dists < 3.
    src_in = src[inliers]
    dst_in = dst[inliers]
    translation = numpy.mean(dst_in - src_in, axis=0)
    transform = numpy.array([[1.,0.,translation[0]], [0.,1.,translation[1]]])
    return transform


def apply_transform(points, transform):
    """Apply 2x3 transformation matrix to points in R2"""
    assert transform.shape == (2, 3)
    points3 = numpy.hstack((points,numpy.ones((points.shape[0], 1))))
    return numpy.dot(points3, transform.transpose()).astype(numpy.float32)


def apply_mat_transform(mat, tr):
    """Apply 2x3 transformation matrix to an image"""
    assert tr.shape == (2, 3)
    return cv2.warpAffine(
        mat.astype(numpy.float32).transpose(), tr, mat.shape).transpose()


def aligned_fits(filename, transform):
    """Apply 2x3 transformation matrix to a fits image, with cache"""
    if transform is None:
        transform = numpy.identity(3)[:2]
    assert transform.shape == (2, 3)
    trhash = hashlib.sha1(transform).hexdigest()[:8]
    storename = "%s.%s.aligned.fits" % (os.path.splitext(filename)[0], trhash)
    if os.path.isfile(storename):
        return readfits.FitsImage(storename).get_data()
    else:
        mat = readfits.FitsImage(filename).get_data()
        aligned = apply_mat_transform(mat, transform)
        readfits.save_fits(storename, aligned)
        return aligned


def match(src, dst):
    """Get the closest point in dst for every src using FLANN."""
    if src.size == 0 or dst.size == 0:
        sze = src.shape[0]
        return (numpy.zeros(sze, numpy.int8), numpy.ones(sze) * numpy.infty)
    FLANN_INDEX_KDTREE = 1  # bug: flann enums are missing
    flann_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=2)
    flann_index = cv2.flann_Index(dst, flann_params)
    indices, dists = flann_index.knnSearch(src, 1, params={})
    # corr = numpy.column_stack((indices, numpy.arange(0,indices.shape[0])))
    # ordered = numpy.array([dst[index] for index in indices.flatten()])
    return (indices[:,0], dists[:,0])


def length(a):
    """Norm of a 2D vector"""
    return math.sqrt(math.pow(a[0], 2) + math.pow(a[1], 2))


def angle(a, b):
    """Angle between two 2D vectors"""
    ang = math.atan2(b[1], b[0]) - math.atan2(a[1], a[0])
    return ang


class AlignedSeq(readfits.Sequence):
    """Iterable aligned sequence"""
    def __init__(self, seq=None, files=None, path=""):
        """AlignedSeq constructor"""
        super(AlignedSeq, self).__init__(seq, files, path)
        self.transforms = store_and_get(seq)
        self.len = sum([1 for t in self.transforms if t is not None])

    def get_element(self, cur):
        """Return image name and affine transformation"""
        #aligned = aligned_fits(self.files[cur], self.transforms[cur])
        #points = apply_transform(features.store_and_get(self.files[cur]),
        #                         self.transforms[cur])
        if self.transforms[cur] is None:
            return None
        else:
            return (self.files[cur], self.transforms[cur])


class NoAlignmentFound(Exception):
    """No alignment found for input"""


def test():
    """Align test"""
    import displayutil
    fits1 = readfits.FitsImage("/home/kevin/test1/dufR_0042.fits")
    fits2 = readfits.FitsImage("/home/kevin/test1/dufR_0045.fits")
    mat1 = fits1.get_data()
    srcpt = features.store_and_get(fits1.filename)[:,:2]
    dstpt = features.store_and_get(fits2.filename)[:,:2]
    src = numpy.array(srcpt).astype(numpy.float32)
    dst = numpy.array(dstpt).astype(numpy.float32)
    transform, dst = ICP(src, dst, True)
    transf = apply_transform(src, transform)
    im = displayutil.Display(size=mat1.shape)
    im.points(srcpt, size=1, color=(0,0,200))
    im.points(dstpt, size=1)
    im.points(transf, size=4, color=(0,0,200))
    im.show()


def test2():
    """Align test"""
    import displayutil
    fits = readfits.FitsImage("/home/kevin/test1/dufR_0042.fits")
    mat1 = fits.get_data()
    srcpt = features.store_and_get(fits.filename)[:,:2]
    src = numpy.array(srcpt).astype(numpy.float32)
    transform1 = numpy.array([[.9982,-.056,30.], [.056,.9982,-30.]])
    dst = apply_transform(src, transform1)
    dstpt = [tuple(d) for d in dst]
    transform = ICP(src, dst, False)
    print transform
    transf = apply_transform(src, transform)
    im = displayutil.Display(size=mat1.shape)
    im.points(srcpt, size=1, color=(0,0,200))
    im.points(dstpt, size=1)
    im.points(transf, size=4, color=(0,0,200))
    im.show()


def stable(seq):
    """Stabilized video of secuence"""
    import displayutil
    video = "output/" + os.path.basename(seq) + "_stable.avi"
    files = readfits.Sequence(seq)
    transforms = store_and_get(seq)
    vw = displayutil.VideoWriter(filename=video, maxr=1200)
    sys.stdout.write("Writing %s" % video)
    for (fi, tr) in zip(files.iterate(), transforms):
        sys.stdout.write(".")
        sys.stdout.flush()
        mat = fi.get_data()
        points = features.store_and_get(fi.filename)[:,:2]
        if tr is not None:
            mat = apply_mat_transform(mat, tr)
            points = apply_transform(points, tr)
        frame = displayutil.Display(mat)
        frame.points(points)
        vw.put_frame(frame.image)

    print "ok, %d frames" % len(files)


def main():
    """Align image sequence"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("SEQ", type=argparse.FileType('r'),
                        help="Sequence to align.")
    parser.add_argument("-f", "--force", action="store_true",
                        help="Ignore cache.")
    parser.add_argument("-p", "--print-transforms", action="store_true",
                        help="print transformation matrixes.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="print debug info.")
    args = parser.parse_args()

    transforms = store_and_get(args.SEQ.name, args.force, args.verbose)
    if args.print_transforms:
        for tr in transforms:
            print tr
            print


if __name__ == "__main__":
    main()
