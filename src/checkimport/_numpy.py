#!/usr/bin/env python

try:
    import numpy
except Exception:
    print "NumPy missing, install with:"
    print "sudo apt-get install python-numpy"
    import sys
    sys.exit(1)
