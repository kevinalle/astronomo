#!/usr/bin/env python

try:
    import os
    BIN = "sex"
    if os.path.isfile("/usr/bin/sextractor"):
        BIN = "sextractor"
    assert(os.system("%s 2>/dev/null >/dev/null" % BIN)==0)
except Exception:
    print "SExtractor missing, install with:"
    print "sudo apt-get install sextractor"
    import sys
    sys.exit(1)
