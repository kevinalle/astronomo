#!/usr/bin/env python

"""
Query USNO-B1 catalog

@author: Kevin Allekotte
"""

from checkimport._numpy import numpy
import urllib


def usno(radeg, decdeg, fovam, n=0, debug=False):
    """Query the USNO-B1 catalog in RA DEC with FOV.

    Get star positions and magnitudes at RA DEC with a FOV field of view from
    catalog USNO-B1 from http://webviz.u-strasbg.fr. As described in:
    http://www.astro.utoronto.ca/~astrolab/files/Lab3_AST325_2012.pdf

    Args:
        radeg: float. RA in decimal degrees/J2000.0.
        decdeg: float. DEC in decimal degrees/J2000.0.
        fovam: float. FOV in arc min.
        n: int. number of stars to return (0 => all).

    Returns:
        features: [(RA, DEC, MAG)] of the n brightest stars (all if n = 0).
    """
    urlbase = 'http://webviz.u-strasbg.fr/viz-bin/asu-tsv/?-source=USNO-B1' \
              '&-c.ra=%4.6f&-c.dec=%4.6f&-c.bm=%4.7f/%4.7f&-out.max=unlimited'
    url = urlbase % (radeg, decdeg, fovam, fovam)
    if debug:
        print "Getting catalog from:\n" + url
    handler = urllib.urlopen(url)
    s = handler.read().strip().splitlines()
    handler.close()
    rad = numpy.array([])  # RA in degrees
    ded = numpy.array([])  # DEC in degrees
    rmag = numpy.array([])  # rmage
    for k in [l for l in s if len(l) > 0 and l[0] != "#"][3:]:
        kw = k.split('\t')
        rad = numpy.append(rad, float(kw[1]))
        ded = numpy.append(ded, float(kw[2]))
        if len(kw) < 13 or kw[12] == '     ':   # No mag is reported
            rmag = numpy.append(rmag, 30.)  # Arbitrary mag if not reported
        else:
            rmag = numpy.append(rmag, float(kw[12]))
    if debug:
        print "Got %d features" % len(rad)
    feats = numpy.vstack((rad, ded, rmag)).T
    ordered = feats[feats[:,2].argsort()]
    if n > 0:
        ordered = ordered[:n]
    return ordered


def main():
    """Get stars from catalog"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("RA", type=float, help="RA in decimal degrees/J2000")
    parser.add_argument("DEC", type=float, help="DEC in decimal degrees/J2000")
    parser.add_argument("FOV", type=float, help="Field of view in arc min")
    parser.add_argument("-n", "--n-brightest", type=int, default=0,
                        help="Display N brightest features (0=ALL)")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="print debug info.")
    args = parser.parse_args()

    feats = usno(args.RA, args.DEC, args.FOV, args.n_brightest, args.verbose)
    for feat in feats:
        print "%f\t%f\t%f" % tuple(feat.tolist())


if __name__ == "__main__":
    main()
