#!/usr/bin/env python

"""
Get atrometric data from Astronomy.net API

@author: Kevin Allekotte
"""

import time
from checkimport._numpy import numpy
from checkimport._pyfits import pyfits
import urllib2
import features
from lib import astrometrynet


def store_and_get(fits, force=False, debug=False):
    """Get astrometric data for image and persist in header"""
    hdulist = pyfits.open(fits)
    header = hdulist[0].header
    if not force and "ALIGNED" in header \
       and header["ALIGNED"] == "Astrometry.net":
        # Already aligned, return stored values
        if debug:
            print "Image previously aligned, returning stored values"
        x0, y0 = header["CRPIX1"], header["CRPIX2"]
        ra, dec = header["CRVAL1"], header["CRVAL2"]
        cd = header["CD1_1"], header["CD1_2"], header["CD2_1"], header["CD2_2"]
        hdulist.close()
        return x0, y0, ra, dec, cd
    else:
        feats = features.store_and_get(fits)
        x0, y0, ra, dec, cd = get_astrometry(feats=feats, debug=debug)
        hdulist.close()
        write_header(fits, x0, y0, ra, dec, cd)
        return x0, y0, ra, dec, cd


def write_header(fits, x0, y0, ra, dec, cd):
    """Write astrometry data to fits header"""
    hdulist = pyfits.open(fits, mode="update")
    header = hdulist[0].header
    header.set("ALIGNED", "Astrometry.net")
    header.set("CRPIX1", x0)
    header.set("CRPIX2", y0)
    header.set("CRVAL1", ra)
    header.set("CRVAL2", dec)
    header.set("CD1_1", cd[0])
    header.set("CD1_2", cd[1])
    header.set("CD2_1", cd[2])
    header.set("CD2_2", cd[3])
    hdulist.flush()


def get_astrometry(feats, debug=False, fitsfile=None):
    """Get astrometry parameters from Astrometry.net.

    Args:
        feats: array. [X, Y] Positions of stars.

    Returns:
        x0: float. Center of alignment in pixels.
        y0: float. Center of alignment in pixels.
        ra: float. Center of plate (RA of point [x0, y0]).
        dec: float. Center of plate (DEC of point [x0, y0]).
        cd: [float]. CD (Coordinate Description) matrix. [c11, c12, c21, c22].
            See http://stsdas.stsci.edu/documents/SUG/UG_21.html
    """
    cli = astrometrynet.Client()
    cli.login("zxrhptnofsaiufsw")  # my API key
    if debug:
        print "Logged in, session:", cli.session

    if feats is not None:
        text = '\n'.join(map(lambda x:"%f\t%f" % (x[0], x[1]), feats[:100,:2]))
        sub = cli.send_request('upload', cli.uploadargs, (None, text))
    else:
        text = open(fitsfile, 'rb').read()
        sub = cli.send_request('upload', cli.uploadargs, (fitsfile, text))
    if sub is None:
        raise AstrometryFailed("Network error")
    subid = sub["subid"]
    if debug:
        print "Submitting with subid:", subid

    jobs = None
    backoff = .1
    while (jobs is None or len(jobs) < 1 or jobs[0] is None) and backoff < 80:
        backoff *= 1.5
        substatus = cli.sub_status(subid)
        if substatus is not None:
            jobs = substatus.get("jobs")
        time.sleep(backoff)

    if jobs is None or len(jobs) < 1:
        raise AstrometryFailed("Service unavailable")
    jobid = jobs[0]
    if debug:
        print "Submission succeeded, processing job:", jobid

    status = "not started"
    backoff = .1
    while status not in ["success", "failure"] and backoff < 15 * 60:
        time.sleep(backoff)
        backoff *= 1.5
        status = cli.job_status(jobid)

    if debug:
        print "Job finished with status:", status
    if status != "success":
        raise AstrometryFailed("Job terminated in %s" % status)

    # cali = cli.get_calibration(jobid)
    wcsurl = "http://" + cli.server + ".astrometry.net/wcs_file/%s"
    wcsreq = urllib2.Request(url=wcsurl % jobid)
    if debug:
        print "Requesting wcs.fits from", wcsurl % jobid
    wcs = pyfits.Header.fromstring(urllib2.urlopen(wcsreq).read())
    x0, y0 = wcs.get("CRPIX1"), wcs.get("CRPIX2")
    ra, dec = wcs.get("CRVAL1"), wcs.get("CRVAL2")
    cd = map(wcs.get, ("CD1_1", "CD1_2", "CD2_1", "CD2_2"))

    if debug:
        print "Got alignment data from fits header:"
        print "RA:", ra, "DEC:", dec, "x0:", x0, "y0:", y0
        print "CD:", cd

    return x0, y0, ra, dec, cd


def maketransform(x0, y0, ra, dec, cd):
    """Make a 3x3 transformation matrix from astrometric parameters.

    Args:
        x0: float. Center of alignment in pixels.
        y0: float. Center of alignment in pixels.
        ra: float. Center of plate (RA of point [x0, y0]).
        dec: float. Center of plate (DEC of point [x0, y0]).
        cd: [float]. CD (Coordinate Description) matrix. [c11, c12, c21, c22].
            See http://stsdas.stsci.edu/documents/SUG/UG_21.html

    Returns:
        transform: Transformation matrix to convert from [X, Y] to [RA, DEC].
    """
    normaliztr = numpy.array([[1, 0, -x0],
                              [0, 1, -y0],
                              [0, 0, 1]], numpy.float32)
    transform2 = numpy.array([[cd[0], cd[1], ra],
                              [cd[2], cd[3], dec],
                              [0, 0, 1]], numpy.float32)
    transform = numpy.dot(transform2, normaliztr)
    return transform


def get_transform(fits):
    """Get transformation matrix to convert X, Y to RA, DEC.

    Args:
        fits: FITS image (filename).

    Returns:
        transform: Transformation matrix to convert from [X, Y] to [RA, DEC].
    """
    x0, y0, ra, dec, cd = store_and_get(fits)
    tr = maketransform(x0, y0, ra, dec, cd)
    return tr


class AstrometryFailed(Exception):
    """Astrometry.net job failed or time exceeded"""


def main():
    """Get astrometric params for fits image."""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("FILE", type=argparse.FileType('r'),
                        help="FITS file to get astrometric data.")
    parser.add_argument("-t", "--transform-matrix", action="store_true",
                        help="output transformation matrix.")
    parser.add_argument("-f", "--force", action="store_true",
                        help="ignore stored values and get new ones.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="print debug info.")
    args = parser.parse_args()

    try:
        params = store_and_get(args.FILE.name, args.force, debug=args.verbose)
        print "x0: %f y0: %f ra: %f dec: %f\nCD: %s" % params
        if args.transform_matrix:
            print maketransform(*params)
    except AstrometryFailed, e:
        print "Astrometry failed:", e


if __name__ == "__main__":
    main()
