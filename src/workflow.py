#!/usr/bin/env python

"""
Data adquisition and reduction workflow.

@author: Kevin Allekotte
"""

import os
import time
import telescope
import ccd
import mount
import pathfinder


def search(ra, dec, interval, exposure):
    """Search for moving objects"""
    _ccd = ccd.SynthCCD(write_astrometry_header=True, sleep=False)
    _mount = mount.FakeMount()
    t = telescope.Telescope(_mount, _ccd, "../stars/")
    today = "%d-%d-%d" % time.localtime()[:3]
    name, suffix = None, 0
    while name is None or os.path.isdir(os.path.join(t.outdir, name)):
        name = "%s_%d" % (today, suffix)
        suffix += 1
    print "Generando secuencia %s" % os.path.join(t.outdir, name)
    seq = t.take_sequence(ra, dec, None, 8, interval, exposure, name)
    print "Buscando objetos moviles"
    objects = pathfinder.find_moving_objects(seq, debug=True)
    print "%d objetos moviles encontrados" % len(objects)
    pathfinder.printRADEC(objects)
    if len(objects) > 0:
        print "Following object 0"
        obs_jd = objects[0]["OBSTIME"] + 2400000.5
        days_since_obs = (telescope.getJD() - obs_jd)
        ra_now = objects[0]["RA"] + objects[0]["dRA"] * days_since_obs
        dec_now = objects[0]["DEC"] + objects[0]["dDEC"] * days_since_obs
        seq2 = t.take_sequence(ra_now, dec_now,
                               [objects[0]["dRA"], objects[0]["dDEC"]],
                               15, interval, exposure, name + "_follow")
        print "Wrote %s" % seq2


def main():
    """Search for moving objects in the sky"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("RA", type=float, help="RA of region to search")
    parser.add_argument("DEC", type=float, help="DEC of region to search")
    parser.add_argument("INTERVAL", type=float, nargs='?', default=2.,
                        help="Time between exposures")
    parser.add_argument("EXPOSURE", type=float, nargs='?', default=1.,
                        help="Exposure time")
    args = parser.parse_args()

    search(args.RA, args.DEC, args.INTERVAL, args.EXPOSURE)

if __name__ == "__main__":
    main()
