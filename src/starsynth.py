#!/usr/bin/env python

"""
Generate a synthetic secuence of fits image with moving objects for testing.

@author: Kevin Allekotte
"""

import os
import sys
import unicodedata
import astrometry
from checkimport._cv2 import cv2
from checkimport._numpy import numpy
from checkimport._pyfits import pyfits

import starcatalog
from align import apply_transform


class StarSynth:
    """Synthetic FITS generator."""
    def __init__(self, **kwargs):
        if not kwargs.get("random"):
            numpy.random.seed(0)
        # Default parameters
        self.size = 800
        self.noise = {"lvl": 30., "var": 20.}
        self.bnoise = {"lvl": 1200., "var": 80.}
        self.bgradient = (-12., 20.)
        self.trans = {"rot": .3, "displ": 1.}
        self.objects = []
        self.stars = []
        self.write_astrometry_header = False
        self.skycoord = (108., 27., 10.)  # ra, dec, fov
        # Override defaults with arguments
        self.__dict__.update(kwargs)

    def stars_from_catalog(self, n=500):
        """Populate with stars from catalog"""
        ra, dec, fov = self.skycoord
        stars = starcatalog.usno(ra, dec, fov * 2., n)
        ras, decs, mag = stars.T
        ras = (ras - ra) * .89 + ra  # Correccion USNO (?)
        flux = 10 ** ((22.5 - numpy.array(mag)) / 2.5)
        self.stars = zip(zip(ras, decs), [3] * len(ras), flux)

    def stars_from_random(self, nstars=300, star_size=None, star_mag=None):
        """Populate with random stars"""
        if star_size is None:
            star_size = {"lvl": 2., "var": .8}
        if star_mag is None:
            star_mag = {"lvl": 2000., "var": 800.}
        for _ in range(nstars):
            pos = self.xy2radec(numpy.random.rand() * self.size,
                                numpy.random.rand() * self.size)
            sigma = max(1., norm(star_size["lvl"], star_size["var"]))
            lumin = max(0., norm(star_mag["lvl"], star_mag["var"]))
            mag = lumin * sigma ** 2 / star_size["lvl"]
            self.stars.append((pos, sigma, mag))

    def add_object(self, mag, size, speed=0., start=None, displ=None):
        """Add moving object"""
        s = self.size
        obj = {"mag": mag, "size": size}
        ang = numpy.random.rand() * 360.
        if start is not None:
            obj["start"] = start
        else:
            dist = norm(.4 * s, s / 8.)
            rotmat = cv2.getRotationMatrix2D((s / 2., s / 2.), ang, 1.)
            startxy = transform_pos((s / 2. - dist, s / 2.), rotmat)
            obj["start"] = self.xy2radec(*startxy)
        if displ is not None:
            obj["step"] = displ
        else:
            dirmat = cv2.getRotationMatrix2D((0, 0), norm(ang - 360., 20.), 1.)
            obj["step"] = transform_pos((speed, 0.), dirmat)
        self.objects.append(obj)

    def gen_sample(self, time, radec=None):
        """Generate image data"""
        s = self.size
        if radec is None:
            radec = self.skycoord[:2]
        angle = norm(0., self.trans["rot"])
        tr = cv2.getRotationMatrix2D((s / 2., s / 2.), angle, 1.)
        tr[:,2] += (norm(0., self.trans["displ"]),
                    norm(0., self.trans["displ"]))
        bnoise1 = norm(self.bnoise["lvl"], self.bnoise["var"], (s, s))
        bnoise2 = norm(self.bnoise["lvl"], self.bnoise["var"], (s, s))
        cv2.GaussianBlur(bnoise2, (5, 5), 0, bnoise2)
        linear = numpy.linspace(-1., 1., num=s)
        gradient = linear * self.bgradient[0] + \
                   linear[:,numpy.newaxis] * self.bgradient[1]
        data = (bnoise1 + bnoise2) / 2 + gradient
        starsl = numpy.zeros((s, s))  # stars layer
        for pos, sigma, mag in self.stars:
            drawStar(self.radec2xy(*pos, radec=radec), sigma, mag, tr, starsl)
        for obj in self.objects:
            pos = numpy.array(obj["start"]) + time * numpy.array(obj["step"])
            posxy = self.radec2xy(*pos, radec=radec)
            drawStar(posxy, obj["size"], obj["mag"], tr, starsl)
        blur = (s // 6) * 2 + 1
        starsl += cv2.GaussianBlur(starsl, (35, 35), 0) * .15
        numpy.fmax(data, starsl, data)
        noisefactor = 1. - norm(self.noise["lvl"], self.noise["var"]) / (s * s)
        rnoise = cv2.threshold(numpy.random.rand(s, s).astype(numpy.float32),
                               noisefactor, 1., cv2.THRESH_BINARY)[1]
        data += cv2.dilate(rnoise, cv2.getStructuringElement(cv2.MORPH_CROSS,\
            (3, 3))) * numpy.random.rand(s, s) * 6000.
        data += cv2.GaussianBlur(data, (blur, blur), 0) * .1
        return data

    def gen_seq(self, name, nsamples, starttime, interval, outdir="synth"):
        """Generate synthetic sequence"""
        filename = makeSafe(unicode(name))
        if not os.path.isdir(outdir):
            os.mkdir(outdir)
        os.chdir(outdir)
        if not os.path.isdir(filename):
            os.mkdir(filename)
        else:
            print >> sys.stderr, filename, "already exists in", outdir
            return
        os.chdir(filename)
        seq = open(filename, 'w')

        for frame in range(nsamples):
            time = frame * interval
            data = self.gen_sample(time)
            fitsfile = "%s.%03d.fits" % (filename, frame)
            astroh = None
            if self.write_astrometry_header:
                astroh = self.skycoord
            makeFITS(fitsfile, data, {"JUL-DATE": time + starttime}, astroh)
            print >> seq, fitsfile
        seq.close()

    def xy2radec(self, x, y, radec=None):
        """Convert X, Y pixel coordinates to RA, DEC"""
        s, (ra0, dec0, fov) = self.size, self.skycoord
        if radec is not None:
            ra0, dec0 = radec
        return (x - s / 2.) * fov / s / 60. + ra0, \
               (y - s / 2.) * fov / s / 60. + dec0

    def radec2xy(self, ra, dec, radec=None):
        """Convert RA, DEC sky coordinates to X, Y"""
        s, (ra0, dec0, fov) = self.size, self.skycoord
        if radec is not None:
            ra0, dec0 = radec
        return (ra - ra0) * 60. * s / fov + s / 2., \
               (dec - dec0) * 60. * s / fov + s / 2.


def makeFITS(filename, data, header, astrometry_header=None):
    """Save image into a FITS file"""
    hdu = pyfits.PrimaryHDU(data)
    for key, val in header.items():
        hdu.header.update(key, val)
    hdu.writeto(filename)
    if astrometry_header is not None:
        size = data.shape
        x0, y0 = size[0] / 2., size[1] / 2.
        ra, dec, fov = astrometry_header
        cd = [fov / size[0] / 120., 0, 0, fov / size[1] / 120.]
        astrometry.write_header(filename, x0, y0, ra, dec, cd)


def drawStar(pos, sigma, mag, tr, surf, debug=False):
    """Blit a Gaussian to an array"""
    sizey, sizex = surf.shape[0] - 1, surf.shape[1] - 1
    if tr is not None:
        pos = map(int,transform_pos(pos, tr))
    star = makeGauss(sigma) * mag
    x, y, w, h = pos[0], pos[1], star.shape[0], star.shape[1]
    surf[min(max(0, x - w / 2), sizex):max(0, min(x + w - w / 2, sizex)),
         min(max(0, y - h / 2), sizey):max(0, min(y + h - h / 2, sizey))] += \
        star[min(w,max(0,-x + w / 2)):max(0, min(sizex - x - w + w / 2,0) + w),
             min(h,max(0,-y + h / 2)):max(0, min(sizey - y - h + h / 2,0) + h)]
    if debug:
        cv2.circle(surf, (pos[1], pos[0]), 20, mag * 10, 2, cv2.CV_AA)


def makeGauss(sigma):
    """Return a gaussian kernel"""
    size = max(int(sigma * 10), 3)
    x = numpy.arange(0, size, 1, float)
    y = x[:,numpy.newaxis]
    x0 = y0 = size // 2
    return numpy.exp(-1.2 * ((x - x0) ** 2 + (y - y0) ** 2) / sigma ** 2)


def genStartingPos(objects, size):
    """Assign a starting position and speed to every object"""
    for obj in objects:
        dist = norm(.4 * size, size / 8.)
        ang = numpy.random.rand() * 360.
        rotmat = cv2.getRotationMatrix2D((size / 2., size / 2.), ang, 1.)
        obj["start"] = transform_pos((size / 2. - dist, size / 2.), rotmat)
        dirmat = cv2.getRotationMatrix2D((0., 0.), norm(ang - 360, 20.), 1.)
        obj["step"] = transform_pos((obj["speed"], 0.), dirmat)


def norm(loc, scale, size=None):
    """Draw random samples from a normal (Gaussian) distribution."""
    if scale == 0.:
        if size is not None:
            return loc
        else:
            return numpy.ones(size) * loc
    else:
        return numpy.random.normal(loc, scale, size)


def transform_pos(pos, tr):
    """Apply transformation to a point"""
    np = numpy.array((pos,))
    return map(float, tuple(apply_transform(np, tr).flat))


def makeSafe(filename):
    """Remove unacceptable chars from filename"""
    uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    lowercase = uppercase.lower()
    ascii_letters = lowercase + uppercase
    digits = '0123456789'
    validFilenameChars = "-_.() %s%s" % (ascii_letters, digits)
    cleaned = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore')
    return ''.join(c for c in cleaned if c in validFilenameChars)


def main():
    """Generate synthetic sequence"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("name", help="Name of the sequence")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="print debug info.")
    args = parser.parse_args()

    synth = StarSynth(skycoord=(108., 27., 10.),write_astrometry_header=True)
    synth.stars_from_catalog()
    synth.add_object(mag=2000., size=1.0, speed=1000.)
    synth.add_object(mag=1000., size=2.5, speed=900.0)
    synth.add_object(mag=1500., size=0.5, speed=800.0)
    synth.add_object(mag=600.0, size=4.0, speed=700.0)
    synth.add_object(mag=1000., size=2.5, speed=500.0)
    synth.add_object(mag=2000., size=0.6, speed=600.0)
    synth.add_object(mag=1500., size=2.0, speed=300.0)
    synth.gen_seq(args.name, 10, 2454321., 1. / 86400.)

if __name__ == "__main__":
    main()
