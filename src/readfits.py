#!/usr/bin/env python

"""
Methods for handling FITS images and sequences.

@author: Kevin Allekotte
"""

import os
from checkimport._numpy import numpy
from checkimport._pyfits import pyfits


class FitsImage:
    """Representation of a FITS image (pyfits wrapper)"""
    def __init__(self, filename):
        """FitsImage constructor"""
        if not os.path.isfile(filename):
            raise IOError("Not such file: %s" % filename)
        self.filename = filename
        self.hdulist = None

    def load(self):
        """FitsImage initializer"""
        if self.hdulist is None:
            self.hdulist = pyfits.open(self.filename)  # memmap=True ?
        return self

    def get_data(self, extension=0):
        """Numpy 2D float array containing the image data"""
        # tambien se puede:
        # data, hdr = getdata('in.fits', extension, header=True)
        self.load()
        return self.hdulist[extension].data.astype(numpy.float32)

    def get_time(self):
        """Return MJD time of image"""
        self.load()
        # Modified Julian Date (JD - 2400000.5) = days since Nov 17, 1858
        try:
            return self.hdulist[0].header["JUL-DATE"] - 2400000.5
        except KeyError:
            raise ValueNotInHeader("JUL-DATE not found in FITS header")

    def get_radec(self):
        """Return RA, DEC of center of plate"""
        self.load()
        try:
            return self.hdulist[0].header['RA'], self.hdulist[0].header['DEC']
        except KeyError:
            raise ValueNotInHeader("RA/DEC not found in FITS header")

    def __del__(self):
        """FitsImage destructor"""
        if self.hdulist is not None:
            self.hdulist.close()


class ValueNotInHeader(KeyError):
    """Value not found in FITS header"""


def save_fits(filename, mat, header=None):
    """Write mat and heafer to fits"""
    pyfits.writeto(filename, mat, header)


class Sequence(object):
    """An iterable sequence of fits images"""
    def __init__(self, seq=None, files=None, path=""):
        """Sequence constructor"""
        self.files = files
        self.cur = 0
        self.path = path
        self.listing = None
        self.len = 0
        if seq is not None:
            self.loadlist(seq)

    def loadlist(self, seq):
        """Secuence Initializer"""
        self.listing = seq
        self.path = os.path.abspath(os.path.dirname(seq))
        files = open(seq).read().strip().split("\n")
        self.files = [os.path.abspath(self.path + "/" + f) for f in files]
        self.len = len(self.files)

    def __len__(self):
        """Length of the sequence"""
        return self.len

    def end(self):
        """True if at end of sequence"""
        return self.files is None or len(self.files) <= self.cur

    def next(self):
        """Next element for iteration"""
        if self.end():
            self.cur = 0
            return None
        else:
            fi = self.get_element(self.cur)
            self.cur += 1
            return fi

    def get_element(self, cur):
        """Return element as FitsImage"""
        return FitsImage(self.files[cur])

    def delcur(self):
        """Delete current element"""
        self.cur -= 1
        del self.files[self.cur]

    def iterate(self):
        """Iterator dor the sequence"""
        self.cur = 0
        while not self.end():
            n = self.next()
            if n is not None:
                yield n
