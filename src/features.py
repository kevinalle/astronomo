#!/usr/bin/env python

"""
Feature Extractor, with SExtractor or DAOPHOT

@author: Kevin Allekotte
"""

import readfits
from checkimport._numpy import numpy
import os
import daophot


def seq_find(seq, method="sex", force=False):
    """Get and cache features of sequences of FITS images."""
    files = readfits.Sequence(seq)
    for fi in files.iterate():
        daoparams = {"kern_size": 10, "sigma": .8, "Hmin": 200.}
        points = store_and_get(fi.filename, method, force, daoparams)
        npoints = points.shape[0]
        print "%s: %d features" % (os.path.basename(fi.filename), npoints)


def store_and_get(fits_file, method="sex", force=False, daoparams=None):
    """Get features of FITS image (with cache)"""
    storename = os.path.splitext(fits_file)[0] + ".features"
    if not force and os.path.isfile(storename):
        features = numpy.loadtxt(storename, numpy.float32)
    else:
        reverse = False
        if method == "dao":
            mat = readfits.FitsImage(fits_file).get_data()
            if daoparams is None:
                daoparams = {}
            features = daophot.find(mat, **daoparams)
            # header = "method: dao"
        else:
            # Get features with SExtractor
            import checkimport._sextractor as _
            from lib import pysex
            cat = pysex.run(fits_file,
                            params=['Y_IMAGE', 'X_IMAGE', 'FLUX_AUTO'],
                            conf_args={'PHOT_APERTURES':5})
            features = numpy.array(zip(*cat), numpy.float32)
            reverse = True
            # mat = readfits.FitsImage(fits_file).get_data()
            # sharp_features = filtersharp(mat, features)
            # percent = 100 - sharp_features.shape[0] * 100 / features.shape[0]
            # print "sharp: filtered %d%% features"%(percent)
            # features = sharp_features
            if len(features) > 0:
                pass#features[:,2] += 22.5  # ZEROPOINT (?)
            # features = features[features[:,2] < 20.]  # why
            # header = "method: sex"
        # Sort features by magnitude
        if len(features) == 0:
            raise NoFeatures("No features found")
        features = features[features[:,2].argsort()]
        if not reverse:
            features = features[::-1]
        numpy.savetxt(storename, features, fmt="%12.6g")
    return features


class NoFeatures(Exception):
    """No features found in image"""


def filtersharp(mat, feats):
    """Filter features with bad sharpness (see DAOPHOT)"""
    H = daophot.H_filter(mat)
    sharpfilter = daophot.sharp_filter(mat, H, 20)
    points = numpy.trunc(feats[:,:2]).astype(int)
    sharp = sharpfilter[points[:,0], points[:,1]]
    return feats[sharp]


def main():
    """Run features"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("FILE", type=argparse.FileType('r'),
                        help="Sequence or FITS to extract features.")
    parser.add_argument("--method", choices=["dao", "sex"], default="sex",
                        help="Method for feature extraction (DAOPHOT or " \
                             "SExtractor).")
    parser.add_argument("-f", "--force", action="store_true",
                        help="Ignore cache.")
    parser.add_argument("--no-subpixel", action="store_true",
                        help="Don't use subpixel in dao.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="print debug info.")
    args = parser.parse_args()

    if os.path.splitext(args.FILE.name)[1] in [".fit", ".fits"]:
        feats = store_and_get(args.FILE.name, method=args.method,
                              force=args.force,
                              daoparams={"subpixel": not args.no_subpixel})
        print feats
    else:
        seq_find(args.FILE.name, method=args.method, force=args.force)

if __name__ == "__main__":
    main()
