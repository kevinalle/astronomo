#!/usr/bin/env python

"""
Methods for displaying FITS images and sequences.

@author: Kevin Allekotte
"""

from checkimport._cv2 import cv2
from checkimport._numpy import numpy
import readfits
import sys
import os


class Display:
    """A displayable object"""
    def __init__(self, mat=None, fits=None, size=None, maxr=1000):
        """Display constructor"""
        self.maxr = maxr
        self.factor = 1.
        self.image = None
        self.outsize = None
        self.shape = None
        if mat is not None:
            self.initialize(mat.shape)
            self.img(mat)
        elif fits is not None:
            self.fitsimg(fits, 1., True)
        elif size is not None:
            self.initialize(size)

    def initialize(self, size):
        """Display Initializer"""
        self.shape = size
        self.factor = min([float(self.maxr) / x for x in self.shape])
        if self.factor < 1.:
            self.outsize = tuple([int(x * self.factor) for x in self.shape])
        else:
            self.outsize = self.shape
            self.factor = 1.
        zeros = numpy.zeros(self.outsize, numpy.uint8)
        self.image = cv2.merge([zeros, zeros, zeros])

    def img(self, mat, alpha=1.):
        """Blit an image (numpy array)"""
        equ = .3 * (mat - numpy.min(mat)) /\
            (numpy.average(mat) - numpy.min(mat))
        equ = numpy.fmin(equ, 1.)
        img = cv2.resize(equ, (self.outsize[1], self.outsize[0]))
        im = cv2.merge([img, img, img])
        cv2.addWeighted(im, alpha * 255., self.image,
                        1., 1., self.image, cv2.CV_8UC3)
        return self

    def fitsimg(self, fits, alpha=1., init=False):
        """Blit a FITS image"""
        fi = readfits.FitsImage(fits)
        mat = fi.get_data()
        if init:
            self.initialize(mat.shape)
        self.img(mat, alpha)

    def points(self, pts, size=10, color=(0,200,0)):
        """Draw circles on given points"""
        for p in map(self.scalepnt, pts):
            cv2.circle(self.image, p, size, color, 1, cv2.CV_AA)
        return self

    def line(self, line, color=(0,200,0), thick=1):
        """Draw a line"""
        last = self.scalepnt(line[0])
        for p in map(self.scalepnt, line[1:]):
            cv2.line(self.image, last, p, color, thick, cv2.CV_AA)
            last = p
        return self

    def lines(self, lines, color=(0,200,0), thick=1):
        """Draw multiple lines"""
        for line in lines:
            self.line(line, color, thick)
        return self

    def scalepnt(self, p):
        """Rescale points to output size"""
        return int(p[1] * self.factor), int(p[0] * self.factor)

    def show(self):
        """Show the image in an OpenCV window"""
        cv2.namedWindow("out")
        cv2.imshow("out", self.image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.waitKey(1)
        cv2.waitKey(1)
        cv2.waitKey(1)
        cv2.waitKey(1)

    def save(self, filename=None):
        if not filename:
            filename = "output/out.png"
        cv2.imwrite(filename, self.image)


class VideoWriter:
    """Output frames as a video"""
    def __init__(self, size=None, filename="output/out.avi", fps=15, maxr=800):
        """VideoWriter constructor"""
        self.file = filename
        self.fps = fps
        self.size = None
        self.maxoutsize = maxr
        self.factor = 1.
        self.writer = None
        self.outsize = None
        if size is not None:
            self.init(size)

    def init(self, size):
        """VideoWriter initializer"""
        outdir = os.path.dirname(self.file)
        if not os.path.isdir(outdir):
            os.mkdir(outdir)
        self.size = size
        self.factor = min([float(self.maxoutsize) / x for x in self.size])
        if self.factor < 1.:
            self.outsize = tuple([int(x * self.factor) for x in self.size])
        else:
            self.outsize = self.size
        self.writer = cv2.VideoWriter(self.file, cv2.cv.FOURCC(*'DIVX'),
                                      self.fps, self.outsize, True)

    def put_frame(self, frame):
        """Write frame to video"""
        shape = (frame.shape[1], frame.shape[0])
        if self.size is None:
            self.init(shape)
        assert shape == self.size
        if self.factor < 1.:
            cv2.resize(frame, self.outsize, frame)
        self.writer.write(frame)

    def __del__(self):
        """VideoWriter destructor"""
        if self.size is not None:
            del self.writer


def openfile(filepath):
    """Use default program of the OS to open a file"""
    import subprocess
    with open(os.devnull, "w") as fnull:
        if sys.platform.startswith('darwin'):
            subprocess.call(('open', filepath), stdout=fnull, stderr=fnull)
        elif os.name == 'nt':
            os.startfile(filepath)
        elif os.name == 'posix':
            subprocess.call(('xdg-open', filepath), stdout=fnull, stderr=fnull)


def histogram_equalization(mat, nbins=25600):
    """Equalize histogram of image"""
    # Get image histogram
    imhist, bins = numpy.histogram(mat, nbins, normed=True)
    cdf = imhist.cumsum()  # cumulative distribution function
    cdf = 255 * cdf / cdf[-1]  # normalize
    # Use linear interpolation of cdf to find new pixel values
    eq = numpy.interp(mat, bins[:-1], cdf)
    return eq


def showseq(seq, maxr=1000):
    """Display a video of a sequence"""
    files = readfits.Sequence(seq)
    vw = VideoWriter(filename="/tmp/tmpseq.avi", maxr=maxr)
    for fi in files.iterate():
        d = Display(fits=fi.filename)
        vw.put_frame(d.image)
    openfile("/tmp/tmpseq.avi")


def main():
    """Run displayutil"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("FILE", type=argparse.FileType('r'),
                        help="Sequence or FITS to display.")
    parser.add_argument("-f", "--show-features", action="store_true",
                        help="Draw features from cache.")
    parser.add_argument("-n", "--n-brightest", type=int, default=0,
                        help="Display N brightest features (0=ALL).")
    parser.add_argument("-p", "--just-points", action="store_true",
                        help="Render only features (black background).")
    parser.add_argument("-s", "--save-as",
                        help="Save image to file.")
    parser.add_argument("--maxr", "-r", type=int, default=1024,
                        help="Max Resolution.")
    args = parser.parse_args()
    if os.path.splitext(args.FILE.name)[1] in [".fit", ".fits"]:
        d = Display(fits=args.FILE.name, maxr=args.maxr)
        if args.just_points:
            d.image *= 0.
        if args.show_features:
            storename = os.path.splitext(args.FILE.name)[0] + ".features"
            if os.path.isfile(storename):
                features = numpy.loadtxt(storename, numpy.float32)
                points = features[:,:2]
                if args.n_brightest > 0:
                    points = points[:args.n_brightest]
                d.points(points)
        if args.save_as is not None:
            d.save(args.save_as)
        else:
            d.show()
    else:
        showseq(args.FILE.name)

if __name__ == "__main__":
    main()
