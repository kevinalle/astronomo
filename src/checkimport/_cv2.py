#!/usr/bin/env python

try:
    import cv2
except Exception:
    print "OpenCV missing, install with:"
    print "sudo apt-get install python-opencv"
    import sys
    sys.exit(1)
