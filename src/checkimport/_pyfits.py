#!/usr/bin/env python

try:
    import pyfits
except Exception:
    print "PyFITS missing, install with:"
    print "sudo apt-get install python-pyfits"
    import sys
    sys.exit(1)
