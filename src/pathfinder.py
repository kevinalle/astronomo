#!/usr/bin/env python

"""
Find features that move linearly at constant speed throughout aligned images.

@author: Kevin Allekotte
"""

import os
from checkimport._numpy import numpy
import align
import features
import astrometry
import readfits
import displayutil
import random


def find_moving_objects(seq, make_video=False, debug=False):
    """Encuentra objetos moviles y reporta en coordenadas astronomicas (RA/DEC)

    Dada una secuencia de fits, devuelve las posiciones iniciales en (RA, DEC)
    y desplazamiento por unidad de tiempo (dias) de features que describen
    trayectorias lineales de velocidad constante.

    Args:
        seq: string. Archivo que contiene la lista de fits a procesar.
        make_video: bool. If True, guarda un video en output/[SEQ]_path.avi.

    Retruns:
        objects: [OBJ]. Lista de objetos moviles encontrados
            OBJ es un diccionario con:
                RA: RA de la posicion inicial del objeto (J2000).
                DEC: DEC de la posicion inicial del objeto (J2000).
                dRA: Desplazamiento en RA por unidad de tiempo (dia).
                dDEC: Desplazamiento en DEC por unidad de tiempo (dia).
                OBSTIME: Hora de la observacion (en Modified Julian Date).
                LUM: Flujo o Magnitud promedio del objeto.
    """
    # Buscamos trayectorias de objetos en las imagenes (en pixeles).
    traces = findpath(seq, make_video=make_video)
    if debug:
        print "Found %d traces" % len(traces)
    # Obtenemos datos astrometricos del primer frame.
    # (las posiciones iniciales en pixeles estan en referencia al primer frame)
    first = readfits.Sequence(seq).next()
    if debug:
        print "Getting astrometric data"
    x0, y0, ra, dec, cd = astrometry.store_and_get(first.filename)
    # Obtenemos la matrix de transformacion de pixels a RA/DEC
    tr = astrometry.maketransform(x0, y0, ra, dec, cd)
    objects = []
    for pos, disp, time, lum in traces:
        # Convertimos la posicion en pixel a coordenadas astronomicas usando tr
        skypos = numpy.dot(numpy.hstack((pos, 1)), tr.T)
        # Convertimos el desplazamiento en pixeles a RA/DEC por dia
        skydisp = numpy.dot(disp, tr[:2,:2].T)
        print tr
        objects.append({"RA": skypos[0], "DEC": skypos[1],
                        "dRA": skydisp[0], "dDEC": skydisp[1],
                        "OBSTIME": time, "LUM": lum})
    return objects


def findpath(seq, make_video=False):
    """Encuentra trayectorias descritas por obj moviles.

    Dada una secuencia de fits, devuelve las posiciones iniciales y velocidades
    en pixeles de features que describen trayectorias lineales de velocidad
    constante.

    Args:
        seq: string. Archivo que contiene la lista de fits a procesar.
        make_video: bool. If True, guarda un video en output/[SEQ]_path.avi.

    Retruns:
        traces: [([X0, Y0], [dX, dY], obstime, lum)]. Lista de posiciones
            iniciales, velocidad, hora de observacion y luminosidad de cada
            trayectoria encontrada.
    """
    aligned = align.AlignedSeq(seq)
    # Todos los features de la secuencia, sacando "duplicados". (X, Y, LUM)
    acum_points = numpy.empty((0,3), numpy.float32)
    # Contador de la cantidad de "repetidos" de cada feature
    counts = numpy.empty(0, numpy.int8)
    # Indice del frame al que pertenece cada feature
    in_frame = numpy.empty(0, numpy.int8)
    # Timestamp de cada frame
    timestamps = []
    for frame, (fits, transform) in enumerate(aligned.iterate()):
        # Leer el timestamp del frame
        timestamps.append(readfits.FitsImage(fits).get_time())
        # Obtener la lista de features (X, Y, LUM)
        feats = features.store_and_get(fits)
        # Corregir X, Y segun la alineacion
        transformed_pts = align.apply_transform(feats[:,:2], transform)
        points = numpy.column_stack((transformed_pts, feats[:,2]))
        # Matchearlos con los puntos anteriores
        indices, dists = align.match(points[:,:2].copy(),
                                     acum_points[:,:2].copy())
        # Si no tiene un match suficientemente cerca, es un punto "nuevo"
        new = points[numpy.nonzero(dists >= 2.)[0]]
        # Cantidad de puntos nuevos
        n_new = new.shape[0]
        # Agregar los nuevos a la lista
        acum_points = numpy.vstack((acum_points, new))
        # Para todos los puntos nuevos, guardar en el indice el frame actual
        in_frame = numpy.concatenate((in_frame,
                                      numpy.ones(n_new, numpy.int8) * frame))
        # Para los puntos "repetidos", aumentar el contador
        counts[numpy.unique(indices[numpy.nonzero(dists < 2.)[0]])] += 1
        # Poner el contador de los puntos nuevos en 1
        counts = numpy.concatenate((counts, numpy.ones(n_new)))
    # Hasta aca tenemos una lista de todos los features de la secuencia, de los
    # cuales hay muchos repetidos, que son los que se encuentran en el mismo
    # lugar en sucesivos frames.
    # La idea es descartar los puntos que se repiten (probablemente estrellas y
    # cosas fijas), y quedarnos solamente con los que aparecen en un frame.
    # (potencialmente son puntos que se mueven de lugar)

    # Indice de los features sin repetidos
    unique = numpy.where(counts == 1)[0]
    # Puntos sin repetidos (aparecen en un solo frame, interesante) (X, Y, LUM)
    outlayers = acum_points[unique]
    # Indice del frame al que pertenece cada punto
    outlayer_frames = in_frame[unique]
    # Separamos los puntos en una lista de puntos en cada frame
    # (points[i] son los puntos interesantes del frame i)
    framechanges = numpy.nonzero(numpy.diff(outlayer_frames))[0] + 1
    points = numpy.split(outlayers, framechanges)

    # Funcion magica que encuentra trayectorias que describen algunos puntos
    # Es una lista de [(posicion inicial, velocidad)] donde posicion incial es
    # un vector (X, Y) para el tiempo inicial (0 por default), y velocidad es
    # un vector (dX, dY) del desplazamiento en cada unidad de tiempo
    traces = get_traces(points, timestamps)

    if make_video:
        # Una lista de puntos donde deberia haber objetos moviles en cada frame
        circ = [[p0 + v * (timestamps[frame] - t) \
                 for (p0, v, t, _) in traces] for frame in range(len(aligned))]
        # Hacemos un video mostrando los objetos moviles encontrados
        video = "output/" + os.path.basename(seq) + "_path.avi"
        print "Output video: %s" % video
        vw = displayutil.VideoWriter(filename=video, maxr=1000)
        for frame, (fits, transform) in enumerate(aligned.iterate()):
            # Alineamos la imagen
            al = align.aligned_fits(fits, transform)
            # Agregamos circulitos a los objetos
            vw.put_frame(displayutil.Display(mat=al).points(circ[frame]).image)

    return traces


def get_traces(points, timestamps):
    """Encuentra trayectorias a partir de puntos de interes.

    Dados puntos de interes (que no permanezcan a lo largo de la secuencia) y
    la hora de cada frame, devuelve las posiciones iniciales y velocidades de
    trayectorias encontradas.

    Args:
        points: [array]. Una lista de puntos de interes en cada frame.
        timestamps: [float]. El tiempo de toma de cada frame.

    Returns:
        traces: [(array, array, float, float)]. La lista de posicion inicial
            [X0, Y0], velocidad [dX, dY], tiempo y la luminosidad de cada
            objeto movil encontrado.
    """
    # Obtenemos listas de puntos colineales (en espacio y tiempo)
    # Cada traza es una lista de (numero de frame, id del punto), de los puntos
    # que conforman la trayectoria
    ptraces = find_traces(points, timestamps)
    traces = []
    for trace in ptraces:
        # Creamos la lista de features a partir de los indices
        # (t = [#frame, #point])
        pnts = [points[t[0]][t[1]] for t in trace]
        # Y la lista de timestamps de cada feature
        times = [timestamps[t[0]]   for t in trace]
        # Obtenemos la posicion inicial y velocidad que mejor aproxima la lista
        # de features
        p0, v, lum = get_trace(pnts, times, t0=times[0])
        traces.append((numpy.array(p0), numpy.array(v), times[0], lum))
    return traces


def get_trace(points, times, t0=0.):
    """Calcula la trayectoria que mejor aproxima la secuencia de puntos.

    Encuentra posicion inicial y velocidad que mejor se ajusta a la lista de
    features dada (con cuadrados minimos).

    Args:
        points: [(X, Y, LUM)]. Lista de puntos aproximadamente colineales.
        times: [float]. La hora de cada punto.
        t0: float. El tiempo considerado para la posicion inicial. La posicion
            del objeto para algun tiempo t se calcularia como:
            p0 + v * (t - t0).

    Returns:
        p0: [X, Y]. Posicion inicial (correspondiente al tiempo t0).
        v: [dX, dY]. Desplazamiento por unidad de tiempo.
        lum: float. Luminosidad promedio de la secuencia.
    """
    # Usamos cuadrados minimos para encontrar la recta en R3 que mejor se
    # ajusta a los puntos dados (X, Y, T)
    # http://stackoverflow.com/questions/2298390/fitting-a-line-in-3d#2304816
    # Separamos los X, los Y y los T en vectores
    x, y = numpy.array(points)[:,:2].T
    t = numpy.array(times)
    # Buscamos la solucion a [x|1][t] = 0
    # (el plano paralelo al eje Y que mejor ajusta los datos)
    A_xt = numpy.vstack((x, numpy.ones(len(x)))).T
    m_xt, c_xt = numpy.linalg.lstsq(A_xt, t)[0]
    # Buscamos la solucion a [y|1][t] = 0
    # (el plano paralelo al eje X que mejor ajusta los datos)
    A_yt = numpy.vstack((y, numpy.ones(len(y)))).T
    m_yt, c_yt = numpy.linalg.lstsq(A_yt, t)[0]
    # Calculamos la posicion para t0 y el vector [dX, dY]
    p0 = [(t0 - c_xt) / m_xt, (t0 - c_yt) / m_yt]
    v = [1. / m_xt, 1. / m_yt]
    lum = numpy.average([p[2] for p in points])
    return p0, v, lum


def find_traces(points, timestamps, err_radius=2.):
    """Encuentra secuencias de puntos colineales a partir de puntos de interes.

    Dada la lista de puntos de interes de cada frame, busca y devuelve
    secuencias de indices de puntos alineados en espacio y tiempo (colineales y
    de velocidad constante).

    Args:
        points: [array]. Puntos de interes de cada frame.
        timestamps: [float]. La hora de cada frame.
        err_radius: float. El radio de tolerancia de colinealidad de puntos.

    Returns:
        objects: [sequencia]. La lista de secuencias encontradas. Cada
            secuencia es una lista de (#frame, #punto) que representa al punto
            points[#frame][#punto].
    """
    objects = []
    n = len(points)
    # (#frame, timestamp, [(X, Y)]) de cada frame
    ftp = zip(range(n), timestamps, [pts[:,:2] for pts in points])
    # Hacemos RanSaC; elegimos 3 frames al azar, buscamos trios colineales en
    # estos frames, y evaluamos que porcentaje de los otros frames tiene un
    # feature en esa trayectoria. Repetimos el proceso unas cuantas veces y
    # mergeamos los resultados de cada prueba.
    for _ in xrange(n):   # n veces parece razonable
        # Elegimos 3 frames al azar junto con sus tiempos y features
        frame, time, pts = zip(*sorted(random.sample(ftp, 3)))
        # La relacion temporal que hay entre los 3 frames
        factor = (time[2] - time[1]) / (time[1] - time[0])
        # Obtenemos los trios de puntos alineados en espacio y tiempo
        # Devuelve una lista de 3-uplas de indices de puntos
        triplets = find_linear_triplets(
            pts[0].copy(), pts[1].copy(), pts[2].copy(), factor, err_radius)
        # Por cada trio crea una posible secuencia que vamos a ir extendiendo
        traces = [zip(frame, tri) for tri in triplets]
        # El vector de desplazamiento entre el frame 0 y el 2 de cada trio
        diff20 = pts[2][triplets[:,2]] - pts[0][triplets[:,0]]
        for f, t, p in [x for x in ftp if x[0] not in frame]:
            # Ahora vemos si el frame contiene un feature colineal a cada trio
            # Relacion de tiempos entre frame 0, 2 y i
            timefactor = (t - time[2]) / (time[2] - time[0])
            # Las posiciones donde deberian estar los features en el frame i
            dest_positions = pts[2][triplets[:,2]] + diff20 * timefactor
            # Obtenemos los puntos mas cercanos a cada posicion calculada
            indices, dists = align.match(dest_positions, p.copy())
            # Para cada trio, deberia haber un feature colineal en el frame i.
            # indices contiene el indice del punto mas cercano a la posicion
            # donde deberia estar, y dist tiene la distancia**2.
            # Si el punto es suficientemente colineal (la dist suficientemente
            # chica), agregamos el punto a la secuencia del trio.
            for triplet, i, d in zip(range(len(triplets)), indices, dists):
                if d < (err_radius + 2.) ** 2:
                    traces[triplet].append((f, i))
        # Hasta aca, encontramos trios colineales y guardamos la lista de todos
        # los features de los otros frames que pertenecen a la trayectoria.
        for trace in traces:
            # Tomamos la secuencia como valida si es colinear en al menos 40%
            # de los frames.
            if len(trace) >= max(4, n * .4):
                # Queda ver si es la misma traza de una secuencia ya vista.
                # Confiamos que es la misma si comparten al menos 2 puntos.
                new = True
                for obj in objects:
                    if len(set(obj) & set(trace)) > 1:
                        # Es una traza repetida, mergeamos las secuencias
                        # (por si encontramos mas putnos que antes)
                        obj = list(set(obj + trace))
                        new = False
                        break
                if new:   # Es una traza nueva, la agregamos
                    objects.append(trace)
    # Devolvemos las secuencias ordenadas por #frame
    for trace in objects:
        trace.sort()
    return objects


def find_linear_triplets(a, b, c, factor, err_radius=2.):
    """Encuentra trios de puntos colineales.

    Dados 3 conjuntos de puntos, encuentra trios de puntos colineales y de
    distancia dada por factor (d(pa, pb) = d(pb, pc) * factor), con tolerancia
    de radio err_radius.

    Args:
        a: array. Conjunto de puntos A, de la forma [X, Y].
        b: array. Conjunto de puntos B, de la forma [X, Y].
        c: array. Conjunto de puntos C, de la forma [X, Y].
        factor: float. La relacion de distancias deseada entre los trios.
        err_radius: float. El radio de tolerancia de colinealidad.
    """
    # Por cada par de puntos de A y B calculamos donde deberia estar su corr C
    # Si ck es colineal a ai y bj => possible_c[i, j] = ck
    possible_c = b + (b - a[:,numpy.newaxis]) * factor
    # Por cada possible_c buscamos el c mas cercano y la distancia a este
    indices, dists = align.match(numpy.concatenate(possible_c), c)
    indices = numpy.reshape(indices, (a.shape[0], b.shape[0]))
    dists = numpy.reshape(dists, (a.shape[0], b.shape[0]))
    mdist = err_radius ** 2  # dist[i,j] = d(possib_c[i,j], c[indices[i,j]])**2
    # Obtenemos las coordenadas (a, b) de los que tienen un c colineal
    ab = numpy.transpose((dists < mdist).nonzero())
    # Armamos los trios (a, b, c) con los c que son colineales a los (a, b)
    triplets = numpy.hstack((ab, indices[dists < mdist][:,numpy.newaxis]))
    return triplets


def printXY(traces):
    """Print traces in X, Y"""
    for i, trace in enumerate(traces):
        print "Trace", i
        print "\tX0:", trace[0][0], "Y0:", trace[0][1]
        print "\tdX:", trace[1][0], "dY:", trace[1][1]
        print "\tTime:", trace[2], "\tLum:", trace[3]


def printRADEC(traces):
    """Print traces in RA, DEC"""
    for i, trace in enumerate(traces):
        print "Object", i
        print "\tRA:", trace["RA"], "DEC:", trace["DEC"]
        print "\tdRA:", trace["dRA"], "dDEC:", trace["dDEC"]
        print "\tTime:", trace["OBSTIME"], "(MJD)", "\tLum:", trace["LUM"]


def main():
    """Find moving objects in sequence"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("SEQUENCE", type=argparse.FileType('r'),
                        help="Sequence to align.")
    parser.add_argument("--coord", choices=["astro", "pixel"], default="astro",
                        help="Report Coordinates in Astronomic coordinates or \
                              in image pixels.")
    parser.add_argument("-m", "--make-video", action="store_true",
                        help="Output video.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="print debug info.")
    args = parser.parse_args()

    if args.coord == "pixel":
        traces = findpath(args.SEQUENCE.name, args.make_video)
        print len(traces), "traces found."
        printXY(traces)
    else:
        traces = find_moving_objects(args.SEQUENCE.name, args.make_video)
        print len(traces), "objects found."
        printRADEC(traces)


if __name__ == "__main__":
    main()
