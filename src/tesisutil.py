#!/usr/bin/env python
#pylint: disable-all

import numpy
import Image
import os
import displayutil

"""
star1 558:578, 118:138
star2 348:366, 122:140
cosmic 80:100, 180:200
starsat 550:595, 615:660
"""

def make_png_and_dat(data, name, x0, x1, y0, y1):
    d = data[y0:y1, x0:x1]
    im = Image.fromarray(displayutil.Display(mat=d, maxr=2000).image)
    if not os.path.isdir("output/%s" % name):
        os.mkdir("output/%s" % name)
    im.save("output/%s/%s.png" % (name, name))
    numpy.savetxt("output/%s/%s.dat" % (name, name), d, "%d", ",")
    out = open("output/%s/%s.lst" % (name, name), "w")
    for x in range(d.shape[0]):
        for y in range(d.shape[1]):
            print >> out, "%d, %d, %d" % (x, y, d[x,y])
    out.close()


def pyx_plot(data, oname="output/plot.eps", xlog=False):
    import pyx
    if xlog:
        g = pyx.graph.graphxy(width=12, x=pyx.graph.axis.log())
    else:
        g = pyx.graph.graphxy(width=12)
    g.plot(pyx.graph.data.points(data, x=1, y=2), [pyx.graph.style.line()])
    g.writeEPSfile(oname)


def pyx_bar_plot(data=None, list_file=None, name="bar"):
    import pyx
    if list_file is not None:
        txt = open(list_file, "r")
        data = []
        for line in txt.readlines():
            if len(line) > 4:
              data.append(map(int, line.strip().split(",")))
    g = pyx.graph.graphxyz(0, 0, size=6, x=pyx.graph.axis.bar(dist=0), y=pyx.graph.axis.bar(dist=0), z=pyx.graph.axis.lin())
    g.plot(pyx.graph.data.data(pyx.graph.data.points(data), xname=1, yname=2, z=3),
           [pyx.graph.style.bar()])
    g.writeEPSfile("output/%s.eps" % name)


def pyx_scatter_plot(data, xname, yname, xmin, xmax, ymin, ymax, oname="output/plot.eps"):
    g = pyx.graph.graphxy(width=12, height=12,
                          x=pyx.graph.axis.lin(min=xmin, max=xmax, title=xname),
                          y=pyx.graph.axis.lin(min=ymin, max=ymax, title=yname))
    g.plot(pyx.graph.data.points(data, x=1, y=2), [pyx.graph.style.symbol(pyx.graph.style.symbol.circle, size=0.1)])
    g.writeEPSfile(oname)


def convert_eps_gray(name, contrast=True):
    import re, math
    eps = open("%s.eps" % name).read()
    color = re.compile(r'^(\d+\.?\d*|\.\d+) (\d+\.?\d*|\.\d+) (\d+\.?\d*|\.\d+) setrgbcolor$', re.MULTILINE)
    if contrast:
        cf = lambda x: min(max(-math.sin(x * 2. * math.pi) / 6. + x, 0.), 1.)
    else:
        cf = lambda x: x
    fun = lambda x: (str(cf(float(x.group(1)))) + " ") * 3 + "setrgbcolor"
    eps2 = re.sub(color, fun, eps)
    eps2 = re.sub(r'^(\d+\.?\d*|\.\d+) setlinewidth$', r'0.1 setlinewidth', eps2, flags=re.MULTILINE)
    open("%s_gray.eps" % name, 'w').write(eps2)
