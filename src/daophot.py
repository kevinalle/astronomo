#!/usr/bin/env python

"""
Implementation of Peter B. Stetson's
"DAOPHOT: A computer program for crowded-field stellar photometry"

@author: Kevin Allekotte
"""

from checkimport._cv2 import cv2
import displayutil
from checkimport._numpy import numpy
import time


def find(D, kern_size=20, sigma=3., Hmin=250., getflux=True, subpixel=False,
         debug=False):
    """Find positions and flux of stars in image"""
    start_time = time.clock()
    H = H_filter(D)
    maximums = local_max(H, Hmin)
    # round
    rounds = round_filter(D, kern_size, sigma)
    max_round_filtered = numpy.logical_and(maximums, rounds)
    # sharp
    sharp = sharp_filter(D, H, 5)
    max_round_sharp_filtered = numpy.logical_and(max_round_filtered, sharp)
    points = as_points(max_round_sharp_filtered)
    if getflux:
        flux = get_flux(D)
        fluxes = flux[zip(*points)][:,numpy.newaxis]
    else:
        fluxes = numpy.zeros((len(points), 1))
    if subpixel:
        subpix_correction = get_subpix(numpy.fmax(H, 0))
        points += subpix_correction[zip(*points)]
    features = numpy.hstack((points, fluxes))

    if debug:
        print "find took", time.clock() - start_time, "seconds"

        print "maximos locales"
        maximums_points = nonzero(maximums)
        print "%d puntos de interes." % len(maximums_points)
        #displayutil.display(maximums, scale=255, tit="out")

        print "roundness filter"
        max_round_points = nonzero(max_round_filtered)
        print "%d puntos de interes." % len(max_round_points)
        #displayutil.display(max_round_filtered, scale=255, tit="out")

        print "sharpness filter"
        #displayutil.display(max_round_sharp_filtered, scale=255, tit="out")
        max_round_sharp_points = nonzero(max_round_sharp_filtered)
        print "%d puntos de interes." % len(max_round_sharp_points)

        invalidos = set([tuple(x) for x in maximums_points]) -\
                    set([tuple(x) for x in points])
        disp = displayutil.Display(mat=D)
        disp.points(points).points(invalidos, color=(0,0,200))
        disp.show()

    return features


def H_filter(D, kern_size=10, sigma=.8):
    """H convolution of the image"""
    kernel = mat_W(kern_size, kern_size, sigma)
    # filter2D hace cualquier cosa con decimales!
    D = numpy.round(D)
    # H
    return cv2.filter2D(D, -1, kernel, borderType=cv2.BORDER_REPLICATE)


def mat_W(sizex, sizey, sigma):
    """W matrix for the H convolution"""
    W = numpy.empty((sizex, sizey), dtype=numpy.float32)
    G = numpy.empty((sizex, sizey), dtype=numpy.float32)
    for i in range(sizex):
        for j in range(sizey):
            di, dj = i - (sizex - 1) / 2., j - (sizey - 1) / 2.
            G[i,j] = numpy.exp(-(di * di + dj * dj) / (2. * sigma * sigma))
    n = sizex * sizey
    sumG = numpy.sum(G)
    sumG2 = numpy.sum(G * G)
    for i in range(sizex):
        for j in range(sizey):
            W[i,j] = (G[i,j] - sumG / n) / (sumG2 - sumG ** 2 / n)
    return W


def get_flux(D, radius=8, ring=13):
    """Get flux of stars. \sum_radius - base, base = mean(\sum_ring)"""
    # filter2D hace cualquier cosa con decimales!
    D = numpy.round(D)
    circ = get_circle(radius)
    ring = get_circle(ring) - get_circle(radius, 2 * ring +1)
    flux = cv2.filter2D(D, -1, circ, borderType=cv2.BORDER_REPLICATE)
    blur = cv2.GaussianBlur(D, (15, 15), 3.5)
    bg = cv2.erode(blur, get_circle(35).astype(numpy.uint8))
    base = cv2.filter2D(bg, -1, ring, borderType=cv2.BORDER_REPLICATE)
    flux = flux - base * numpy.sum(circ) / numpy.sum(ring)
    flux[flux < 0] = 0
    return flux


def get_subpix(H, radius=3):
    """Get subpixel correction for each pixel neighborhood."""
    # filter2D hace cualquier cosa con decimales!
    H = numpy.round(H) + 1.
    circ = get_circle(radius)
    ch, cw = circ.shape
    circ_x = circ * numpy.arange(-radius, radius + 1) * numpy.ones((ch, 1))
    circ_y = circ * (numpy.ones((cw, 1)) * numpy.arange(-radius, radius + 1)).T
    circ_x, circ_y = circ_x.astype(numpy.float32), circ_y.astype(numpy.float32)
    sub_x = cv2.filter2D(H, -1, circ_x, borderType=cv2.BORDER_REPLICATE)
    sub_y = cv2.filter2D(H, -1, circ_y, borderType=cv2.BORDER_REPLICATE)
    norm = cv2.filter2D(H, -1, circ, borderType=cv2.BORDER_REPLICATE)
    return numpy.array([sub_x / norm, sub_y / norm]).T


def get_circle(radius, size=None):
    if size is None:
        size = 2 * radius + 1
    circle = numpy.zeros((size, size), numpy.float32)
    cv2.circle(circle, (size / 2, size / 2), radius, 1, -1)
    return circle


def local_max(mat, Hmin=200.):
    """Find points that are local maxima of the matrix"""
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    mfloat = cv2.dilate(mat, kernel)
    mint = cv2.compare(mat, mfloat, cv2.CMP_EQ)
    mfloat = 1 / 255. * mat * mint
    _, mint = cv2.threshold(mfloat, Hmin, 1., cv2.THRESH_BINARY)
    return mint.astype(numpy.bool)


def nonzero(mat):
    """Devuelve una lista de las coordenadas no nulas"""
    return numpy.transpose(mat.nonzero())


def as_points(mat):
    """Get coordinates of nonzero points in array"""
    return nonzero(mat).astype(numpy.float32)


def sharp_filter(mat_D, mat_H, size):
    """Page 197, sharp"""
    M = numpy.empty((size, size), dtype=numpy.float32)
    M.fill(1. / (size * size - 1))
    M[size / 2, size / 2] = 0.
    filtered = cv2.filter2D(mat_D, -1, M, borderType=cv2.BORDER_REPLICATE)
    sharpness = cv2.divide(mat_D - filtered, mat_H)

    # Adequate sharpness for a real star is 0.2 < sharp < 1.0
    _, sharpness = cv2.threshold(sharpness, 1.0, 1.0, cv2.THRESH_TOZERO_INV)
    _, sharp = cv2.threshold(sharpness, 0.2, 1.0, cv2.THRESH_BINARY)
    return sharp.astype(numpy.bool)


def round_filter(mat_D, size, sigma):
    """Page 199, round"""
    Wx = mat_W(size, 1, sigma)
    Wy = mat_W(1, size, sigma)
    hx = cv2.filter2D(mat_D, -1, Wx, borderType=cv2.BORDER_REPLICATE)
    hy = cv2.filter2D(mat_D, -1, Wy, borderType=cv2.BORDER_REPLICATE)
    hymhx = hy - hx
    roundness = cv2.divide(2 * hymhx, (hy + hx))

    # Adequate roundness for a real star is -1 < round < +1
    _, rnd = cv2.threshold(numpy.abs(roundness), 1., 1., cv2.THRESH_BINARY_INV)
    return rnd.astype(numpy.bool)
