#!/usr/bin/env python

"""
Telescope control

@author: Kevin Allekotte
"""

import os
import time
from checkimport._pyfits import pyfits
from lib import jdcal


class Telescope:
    """Telescope drivers"""

    def __init__(self, mount_driver, ccd_driver, outdir="stars"):
        """Initialize telescope with mount and ccd drivers"""
        self.mount = mount_driver
        self.ccd = ccd_driver
        self.outdir = None
        self.set_outdir(outdir)

    def set_outdir(self, outdir):
        """Change output directory"""
        self.outdir = outdir
        if not os.path.isdir(self.outdir):
            os.mkdir(self.outdir)

    def take_single(self, ra, dec, seconds, filename):
        """Move to RA/DEC, take single exposure and save FITS file"""
        self.ccd.set_dir(self.outdir)
        self.mount.moveto(ra, dec)
        header = {"ra": ra, "dec": dec, "observer": "Astronomo v1.0"}
        self.ccd.expose(filename, seconds, header)
        return "%s/%s" % (self.outdir, filename)

    def take_sequence(self, ra, dec, displace, n, interval, exposure, name):
        """Make a sequence of exposures.

        Take a sequence <name> of <n> observations starting at <ra>/<dec> every
        <interval> seconds, while moving <displace> (ra/day, dec/day).

        Args:
            ra: RA in degrees of start of sequence.
            dec: DEC in degrees of start of sequence.
            displace: (RA/day, DEC/day). Displacement of each subsequent frame.
            n: Number of frames.
            interval: Time between frames.
            exposure: Exposure time in seconds of each frame.
            name: Output name of sequence.

        Returns:
            list_filename: A file containing the list of FITS.
        """
        start = time.time()
        dirname = os.path.join(self.outdir, name)
        self.ccd.set_dir(dirname)
        list_filename = os.path.join(dirname, name)
        fits_list = open(list_filename, 'w')
        self.mount.moveto(ra, dec)
        for frame in range(n):
            frame_time = time.time()
            if displace is not None:
                days_since_start = (frame_time - start) / 86400.
                frame_ra = ra + displace[0] * days_since_start
                frame_dec = dec + displace[1] * days_since_start
                self.mount.moveto(frame_ra, frame_dec)
            else:
                frame_ra, frame_dec = ra, dec
            fitsname = "%s.%03d.fits" % (name, frame)
            filename = os.path.join(dirname, fitsname)
            jd = getJD()
            header = {"ra": frame_ra, "dec": frame_dec, "time": jd,
                      "observer": "Astronomo v1.0"}
            self.ccd.expose(filename, exposure, header)
            hdulist = pyfits.open(filename, mode="update")
            fitsheader = hdulist[0].header
            fitsheader.set("JUL-DATE", jd)
            hdulist.flush()
            fits_list.write("%s\n" % fitsname)
            time.sleep(max(0, interval - (time.time() - frame_time)))
        fits_list.close()
        return list_filename


def getJD(t=None):
    """Get current Modified Julian Date (with presicion up to seconds)"""
    gmt = time.gmtime(t)
    return sum(jdcal.gcal2jd(gmt.tm_year, gmt.tm_mon, gmt.tm_mday)) + \
        gmt.tm_hour / 24. + gmt.tm_min / 1440. + gmt.tm_sec / 86400.
