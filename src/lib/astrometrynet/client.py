import os
import sys
import base64
from urllib2 import urlopen
from urllib2 import Request
from urllib2 import HTTPError
from urllib import urlencode
from urllib import quote
from exceptions import Exception
from cStringIO import StringIO

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.application  import MIMEApplication
from email.generator import Generator
from email.encoders import encode_noop

from api_util import json2python, python2json


class MalformedResponse(Exception):
    pass


class RequestError(Exception):
    pass


class Client(object):
    def __init__(self, server="nova"):
        self.session = None
        self.server = server
        self.apiurl = "http://%s.astrometry.net/api/" % self.server
        self.uploadargs = self._get_upload_args()

    def get_url(self, service):
        return self.apiurl + service

    def send_request(self, service, args, file_args=None, debug=False):
        '''
        service: string
        args: dict
        '''
        if self.session is not None:
            args.update({ 'session' : self.session })
        json = python2json(args)
        url = self.get_url(service)
        if debug:
            print 'Python:', args
            print 'Sending json:', json
            print 'Sending to URL:', url

        if file_args is not None:
            # If we're sending a file, format a multipart/form-data
            headers, data = multipartformdata(json, file_args)
        else:
            # Else send x-www-form-encoded
            headers, data = {}, urlencode({'request-json': json})

        request = Request(url=url, headers=headers, data=data)

        try:
            txt = urlopen(request).read()
            result = json2python(txt)
            stat = result.get('status')
            if debug:
                print 'Got json:', txt
                print 'Got result:', result
                print 'Got status:', stat
            if stat == 'error':
                errstr = result.get('errormessage', '(none)')
                raise RequestError('server error message: ' + errstr)
            return result
        except HTTPError, e:
            if debug:
                print 'HTTPError', e
            return None

    def login(self, apikey):
        args = { 'apikey' : apikey }
        result = self.send_request('login', args)
        sess = result.get('session')
        # print 'Got session:', sess
        if not sess:
            raise RequestError('no session in result')
        self.session = sess

    def _get_upload_args(self, **kwargs):
        args = {}
        for key,default,typ in [('allow_commercial_use', 'd', str),
                                ('allow_modifications', 'd', str),
                                ('publicly_visible', 'y', str),
                                ('scale_units', None, str),
                                ('scale_type', None, str),
                                ('scale_lower', None, float),
                                ('scale_upper', None, float),
                                ('scale_est', None, float),
                                ('scale_err', None, float),
                                ('center_ra', None, float),
                                ('center_dec', None, float),
                                ('radius', None, float),
                                ('downsample_factor', None, int),
                                ]:
            if key in kwargs:
                val = kwargs.pop(key)
                val = typ(val)
                args.update({key: val})
            elif default is not None:
                args.update({key: default})
        return args
    
    def url_upload(self, url, **kwargs):
        args = dict(url=url)
        args.update(self._get_upload_args(**kwargs))
        result = self.send_request('url_upload', args)
        return result

    def upload(self, fn, **kwargs):
        args = self._get_upload_args(**kwargs)
        try:
            f = open(fn, 'rb')
            result = self.send_request('upload', args, (fn, f.read()))
            return result
        except IOError:
            print 'File %s does not exist' % fn     
            raise

    def job_status(self, job_id):
        result = self.send_request('jobs/%s' % job_id, {})
        stat = result.get('status')
        return stat

    def get_calibration(self, job_id):
        """Only call if job staus is success"""
        result = self.send_request('jobs/%s/calibration' % job_id, {})
        return result

    def sub_status(self, sub_id):
        result = self.send_request('submissions/%s' % sub_id, {})
        if result is None:
            return None
        else:
            return dict(result)


def multipartformdata(json, file_args):
    m1 = MIMEBase('text', 'plain')
    m1.add_header('Content-disposition', 'form-data; name="request-json"')
    m1.set_payload(json)

    m2 = MIMEApplication(file_args[1],'octet-stream',encode_noop)
    m2.add_header('Content-disposition',
                  'form-data; name="file"; filename="%s"' % file_args[0])
    mp = MIMEMultipart('form-data', None, [m1, m2])

    fp = StringIO()
    g = MyGenerator(fp)
    g.flatten(mp)
    data = fp.getvalue()
    headers = {'Content-type': mp.get('Content-type')}
    return headers, data


class MyGenerator(Generator):
    # Makie a custom generator to format it the way we need.
    def __init__(self, fp, root=True):
        Generator.__init__(self, fp, mangle_from_=False,
                           maxheaderlen=0)
        self.root = root
    def _write_headers(self, msg):
        # We don't want to write the top-level headers;
        # they go into Request(headers) instead.
        if self.root:
            return                        
        # We need to use \r\n line-terminator, but Generator
        # doesn't provide the flexibility to override, so we
        # have to copy-n-paste-n-modify.
        for h, v in msg.items():
            print >> self._fp, ('%s: %s\r\n' % (h,v)),
        # A blank line always separates headers from body
        print >> self._fp, '\r\n',

    # The _write_multipart method calls "clone" for the
    # subparts.  We hijack that, setting root=False
    def clone(self, fp):
        return MyGenerator(fp, root=False)

